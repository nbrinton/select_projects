import java.io.*;
public class WriteTest_luke {
	public static void main(String [] args) {
		try {
		RandomAccessFile file = new RandomAccessFile("test.bin","rw");
		BTreeNode_luke node = new BTreeNode_luke(2);
		node.setOffset(file.getFilePointer());
		node.initBTOs();
		System.out.println("Node before write and read:");
		node.printNode();
		System.out.println("Node after write and read:");
		node.write(file);
		node.read(file,0);
		node.printNode();
		System.out.println("file written successfully");
		}catch(IOException i) {
			System.out.println("IOException occurred...");
		}
	}
}

