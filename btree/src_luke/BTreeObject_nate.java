/**
 * Created by nbrinton on 4/26/16.
 */

import java.io.RandomAccessFile;
import java.io.IOException;

public class BTreeObject_nate {
	public long key;
	public long freqCount;

	public BTreeObject_nate(long key/*, currentFileOffset */) {
		this.key = key;
		this.freqCount = 1;
	}

	public BTreeObject_nate() {
		this.key = -1;
		this.freqCount = 1;
	}

	public void incrementFrequency() {
		this.freqCount++;
	}


	//These read and write methods for the BTreeObject are necessary for the implementation using BTreeObjects
	public void read(RandomAccessFile binaryFile) throws IOException {
		this.key = binaryFile.readLong();
		this.freqCount = binaryFile.readLong();
	}

	public void write(RandomAccessFile binaryFile) throws IOException {
		binaryFile.writeLong(key);
		binaryFile.writeLong(freqCount);
	}

	public boolean contains(long queryKey) {
		if(this.key == queryKey)
			return true;
		else
			return false;
	}

	public String toString(){
		String str = "";
		str += key;
		return str;
	}
}
