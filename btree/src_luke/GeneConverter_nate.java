/**
 * Created by nbrinton on 4/19/16.
 */
public class GeneConverter_nate {
	private int seqLength;

	public GeneConverter_nate(int seqLength) {
		this.seqLength = seqLength;
	}


	/**
	 * This method generates a long key given a sequence string
	 *
	 * @param str
	 * @return
	 */
	public long stringToKey(String str) {
		long result = 0;
		if(str == null)
			return 0;
		str = str.toUpperCase();
		for (int i = 0; i < seqLength; i++) {
			long mask = 0;
			switch (str.substring(i, i + 1)) {
				case "A":
					mask = 0;
					break;
				case "C":
					mask = 1;
					break;
				case "T":
					mask = 3;
					break;
				case "G":
					mask = 2;
					break;
			}
			result = result << 2;
			result = result | mask;
		}
		return result;
	}

	public String keyToString(long key) {
		String result = "";
		for (int i = seqLength; i > 0; i--) {
			long iKey = key >> i * 2;

			if (iKey == 0) {
				result += "A";
			} else if (iKey == 1) {
				result += "C";
			} else if (iKey == 2) {
				result += "G";
			} else if (iKey == 3) {
				result += "T";
			}
		}

		return result;
	}

}
