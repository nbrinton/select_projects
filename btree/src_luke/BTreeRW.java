import java.io.RandomAccessFile;
import java.io.IOException;
/**
 * Created by nbrinton on 4/16/16.
 */
public class BTreeRW {
	private int t;//degree
	private int maxNumKeys;
	private int maxNumChildren;
	private BTreeNode_nate root;
	private RandomAccessFile binaryFile;
	private boolean debug;


	//Constructor
	public BTreeRW(int degree, boolean debug,RandomAccessFile binaryFile) {
		this.debug = debug;
		this.t = degree;
		this.binaryFile = binaryFile;
		maxNumKeys = (2 * t) - 1;
		maxNumChildren = 2 * t;
		//possibly initialize root here. Or, initialize in a first case of insert()

	}

	public void insert(long akey) throws IOException  {
		//if root is null, then create a new BTreeNode_nate for the root (to keep in memory)
		if (root == null) {
			root = new BTreeNode_nate();
			root.setOffset(binaryFile.length());
			root.write(root.offset);
		}
		root.insert(akey);
	}

	public long search(long akey) {
		BTreeNode_nate aNode = new BTreeNode_nate();
		aNode.read(0);
		long result = searcher(aNode,akey);
		return result;
	}

	public long searcher(BTreeNode_nate aNode,long akey) {
		int i = 0;
		while(i < aNode.getNumKeys() && akey > aNode.keys[i]) {
			i++;
		}
		if(i < aNode.keys[i] && akey == aNode.keys[i])
			return aNode.freqs[i];
		else if(aNode.isLeaf())
			return 0;
		else {
			aNode.read(aNode.children[i]);
			return searcher(aNode,akey);
		}
	}


	/**
	 * NOTE: make private before completion!!!
	 * Created by nbrinton on 4/16/16.
	 */
	public class BTreeNode_nate {
		private long[] keys;//Array of BTreeObject_nates that tracks the keys and frequency of each key
		private long[] freqs;
		private long[] children;//Offset values
		//private BTreeNode_nate childrenNodes[];//Direct descendant children BTreeNode_nates of this node
		private long offset;

		//Constructor
		public BTreeNode_nate(/*long offset*/) {
			//initializing constants
			keys = new long[maxNumKeys];
			freqs = new long[maxNumKeys];
			children = new long[maxNumChildren];
			//childrenNodes = new BTreeNode_nate[maxNumChildren];
		}

		public boolean isLeaf() {
			if(children[0] != 0)
				return false;
			else
				return true;
		}

		public int getNumKeys() {
			int numKeys = 0;
			for(int i = 0; i < this.keys.length; i++) {
				if(this.freqs[i] != 0)
					numKeys++;
			}
			return numKeys;
		}

		public int getNumChildren() {
			int numChildren = 0;
			for(int i = 0; i < this.children.length; i++) {
				if(this.children[i] != 0)
					numChildren++;
			}
			return numChildren;
		}

		public String toString(int height, int childNum) {
			String str = "";
			if (this == root) {
				str += "root\n";
			} else {
				str += "|";
				String indent = "";
				for (int i = 0; i < height; i++) {
					indent += "-";
				}
				str += indent + "> c (" + height + ", " + childNum + ") ";
			}
			str += "[ ";
			childNum = 0;
			for (int i = 0; i < this.getNumKeys(); i++) {
				str += this.keys[i] + ", ";
			}
			str += " ]\n";

			for (int i = 0; i < this.children.length; i++) {
				childNum++;
				if (children[i] != 0) {
					BTreeNode_nate child = new BTreeNode_nate();
					child.read(this.children[i]);
					str += child.toString((height + 1), (childNum));
				} else {
					height = 0;
				}
			}
			return str;
		}

		/**
		 * Sets this nodes' file offset
		 * Must be set before writing to file.
		 * @param offset file offset
		 */
		public void setOffset(long fileOffset) {
			this.offset = fileOffset;
		}


		/**
		 * Writes this node to a binary file (RandomAccessFile)
		 * @param binaryFile file to write to
		 */
		public void write(long fileOffset) {
			try {
			binaryFile.seek(fileOffset);
			binaryFile.writeLong(fileOffset); //write fileoffset
			for(int i = 0; i < this.keys.length; i++){ //write keys
				binaryFile.writeLong(this.keys[i]);
			}
			for(int i = 0; i < this.freqs.length; i++) { //write freqs
				binaryFile.writeLong(this.freqs[i]);
			}
			for(int i = 0; i < this.children.length; i++) //write children offsets
				binaryFile.writeLong(this.children[i]);
			}catch(IOException e) {
				System.out.println("IOException occurred in write");
			}
		}

		public void read(long fileOffset) {
			try{
			binaryFile.seek(fileOffset); //seek to the file offset
			this.offset = binaryFile.readLong(); //read file offset
			for(int i =0; i < this.keys.length; i++) { //read keys
				this.keys[i] = binaryFile.readLong();
			}
			for(int i = 0; i < this.freqs.length; i++) { //read freqs
				freqs[i] = binaryFile.readLong();
			}
			for(int i = 0; i < this.children.length; i++) //read children offsets
				this.children[i] = binaryFile.readLong();
			}catch(IOException e) {
				System.out.println("IOException occurred in read");
				System.out.println(e.getMessage());
				System.out.println(fileOffset);
			}
		}

		private void splitChild(BTreeNode_nate parent, int splitIndex) throws IOException {
			if (debug == true) {
				System.out.println("Entered splitChild( )");
			}
			BTreeNode_nate right = new BTreeNode_nate();
			right.setOffset(binaryFile.length());
			System.out.println("right splitchild offset: " + right.offset);
			right.write(right.offset);
			BTreeNode_nate child = new BTreeNode_nate();
			child.read(parent.children[splitIndex]);
			child.write(child.offset);

			for (int i = 0; i < t - 1; i++) {
				right.keys[i] = child.keys[i + t];
				right.freqs[i] = child.freqs[i + t];
			}

			if (child.isLeaf() == false) {
				for (int i = 0; i < t; i++) {
					right.children[i] = child.children[i + t];
				}
			}

			for (int i = parent.getNumKeys(); i > splitIndex; i--) { //was splitIndex + 1
				parent.children[i + 1] = parent.children[i];
			}

			parent.children[splitIndex + 1] = right.offset;

			for (int i = parent.getNumKeys() - 1; i >= splitIndex; i--) {
				parent.keys[i + 1] = parent.keys[i];
				parent.freqs[i + 1] = parent.freqs[i];
			}

			parent.keys[splitIndex] = child.keys[t - 1];
			parent.freqs[splitIndex] = child.freqs[t - 1];

			//Clear the old values from the right of the child node
			for(int i = t; i < maxNumChildren; i++) {
				child.children[i] = 0;
			}
			for (int i = t - 1; i < maxNumKeys; i++) {
				child.keys[i] = 0;
				child.freqs[i] = 0;
			}

			child.write(child.offset);
			right.write(right.offset);
			parent.write(parent.offset);
		}

		private void insertNonFull(BTreeNode_nate currentNode, long akey) throws IOException {
			if (debug == true) {
				System.out.println("Entered insertNonFull( )");
				System.out.println("# keys = " + currentNode.getNumKeys());
			}
			int i = currentNode.getNumKeys() - 1;//-1 for the index offset not given by the book

			if (currentNode.isLeaf() == true) {
				if (debug == true) {
					System.out.println("Node is Leaf:");
				}
				if (i >= 0 && akey < currentNode.keys[i]) {
					if (debug == true) {
						System.out.println("Shifting keys to the right...");
					}
				}

				if (i > -1 && currentNode.contains(akey)) {
					currentNode.freqs[currentNode.indexOf(akey)]++;
					return;
				}

				while (i >= 0 && akey < currentNode.keys[i]) {
					currentNode.keys[i + 1] = currentNode.keys[i];
					currentNode.freqs[i + 1] = currentNode.keys[i];
					i--;
				}

				currentNode.keys[i + 1] = akey;
				currentNode.freqs[i + 1] = 1;
				if (debug == true) {
					System.out.println("KEY INSERTED: " + akey);
				}

				currentNode.write(currentNode.offset);//write to leaf 1st change

				for(long l : currentNode.keys)
					System.out.println(l);

			} else {
				if (i > -1 && currentNode.contains(akey)) {
					currentNode.freqs[currentNode.indexOf(akey)]++;
					return;
				}

				if (debug == true) {
					System.out.println("Node NOT Leaf:");
				}

				if (i >= 0 && akey < currentNode.keys[i]) {
					if (debug == true) {
						System.out.println("Shifting the index. (i = " + i + ")");
					}
				}

				while (i >= 0 && akey < currentNode.keys[i]) {
					i--;
				}

				if (debug == true) {
					System.out.println("(i = " + i + ")");
				}

				i++;

				if (debug == true) {
					System.out.println("Increment i: \n(i = " + i + ")");
				}

				BTreeNode_nate child = new BTreeNode_nate();
				child.read(currentNode.children[i]);

				if (child.getNumKeys() == maxNumKeys) {
					splitChild(currentNode, i);
					i = 0; //added this to look at all keys to find right child.
					while(currentNode.freqs[i] >= 1 && akey > currentNode.keys[i]){ //changed from if
						i++;
						if(i >= currentNode.children.length - 1)
							break;
					}
					child.read(currentNode.children[i]);
				}
				insertNonFull(child, akey);
			}
		}

		public void insert(long akey) throws IOException {
			if (debug == true) {
				System.out.println("INSERTING: " + akey);
			}
			//CASE 1: insert on root as a leaf when root is full
			if (root.getNumKeys() == maxNumKeys) {
				if (debug == true) {
					System.out.println("Node is full: ");
				}
				BTreeNode_nate oldRoot = new BTreeNode_nate();
				oldRoot.read(root.offset);
				oldRoot.setOffset(binaryFile.length());
				oldRoot.write(oldRoot.offset);
				BTreeNode_nate newRoot = new BTreeNode_nate();
				newRoot.children[0] = oldRoot.offset;
				newRoot.write(root.offset);
				root = newRoot;
				//testing null pointer in splitchild

				BTreeNode_nate btn = new BTreeNode_nate();
				btn.read(newRoot.children[0]);
				for(int i = 0; i < btn.getNumKeys(); i++) {
					if(i == 0)
						System.out.println("Entered child keys loop");
					System.out.println(btn.keys[i]);
				}
				//done testing

				splitChild(newRoot, 0);
				insertNonFull(newRoot, akey);
			} else {
				insertNonFull(root, akey);
			}
		}

		public boolean contains(long akey) {
			for (int i = 0; i < this.keys.length; i++) {
				if (this.freqs[i] >= 1 && this.keys[i] == akey)
					return true;
			}
			return false;
		}

		public int indexOf(long akey) {
			for (int i = 0; i < this.keys.length; i++) {
				if (this.freqs[i] >= 1 && this.keys[i] == akey)
					return i;
			}
			return -1;
		}


	}

	public String toString() {
		String str = "";
		str += root.toString(0, 0);
		return str;
	}

}
