import java.io.*;
import java.util.Random;
	/**
	 * NOTE: make private before completion!!!
	 * Created by nbrinton on 4/16/16.
	 */
	public class BTreeNode_luke {
		private int numKeys;
		private int numChildren;
		private long offset;
		public BTreeObject_nate[] keys;//Array of BTreeObjects that tracks the keys and frequency of each key
		public long[] children;//Offset values
		private boolean isLeaf;
		private BTreeNode_luke childrenNodes[];//Direct descendant children BTreeNodes of this node

		//Constructor
		public BTreeNode_luke(int degree) {
			int maxNumKeys = (2 * degree) - 1;
			int maxNumChildren = 2 * degree;
			//initializing constants
			keys = new BTreeObject_nate[maxNumKeys];
			children = new long[maxNumChildren];
			childrenNodes = new BTreeNode_luke[maxNumChildren];

			//initializing variables
			this.offset = 0;
			numKeys = 0;
			numChildren = 0;
		}

		public void initBTOs() {
			Random rand = new Random();
			for(int i = 0; i < keys.length; i++) {
				keys[i] = new BTreeObject_nate(rand.nextLong());
			}
		}

		/**
		 * Set's the leaf status of this BTreeNode
		 *
		 * @param leafStatus
		 */
		public void setLeaf(boolean leafStatus) {
			this.isLeaf = leafStatus;
		}

		/**
		 * Sets the node's fileoffset
		 * @param offset
		 */
		public void setOffset(long offset) {
			this.offset = offset;
		}

		/**
		 * Writes this node to a binary file (RandomAccessFile)
		 * @param binaryFile file to write to
		 */
		public void write(RandomAccessFile binaryFile) {
			try {
			binaryFile.seek(offset); //seek to the file offset
			for(BTreeObject_nate bto: keys) //write BTreeObject keys
				bto.write(binaryFile);
			for(long l : children) //write children offsets
				binaryFile.writeLong(l);
			}catch(IOException e) {
				System.out.println("IOException occurred...");
			}
		}

		public void read(RandomAccessFile binaryFile,long offset) {
			try{
			binaryFile.seek(offset); //seek to the file offset
			for(BTreeObject_nate bto: keys) //write BTreeObject keys
				bto.read(binaryFile);
			for(long l : children) //write children offsets
				l = binaryFile.readLong();
			}catch(IOException e) {
				System.out.println("IOException occurred...");
			}
		}

		public void printNode() {
			for(BTreeObject_nate bto: keys) //write BTreeObject keys
				System.out.println(bto);
			for(long l : children) //write children offsets
				System.out.println(Long.toBinaryString(l));
		}

/*

		private void splitChild(BTreeNode parent, int splitIndex) {
			BTreeNode right = new BTreeNode();
			BTreeNode child = parent.childrenNodes[splitIndex];
			right.isLeaf = child.isLeaf;
			right.numKeys = t - 1;

			for (int i = 0; i < t - 1; i++) {   //was just t
				right.keys[i].key = child.keys[i + t].key;
			}

			if (child.isLeaf == false) {
				for (int i = 0; i < t; i++) {
					right.childrenNodes[i] = child.childrenNodes[i + t];
				}
			}

			child.numKeys = t - 1;

			for (int i = parent.numKeys; i > splitIndex; i--) { //was splitIndex + 1
				parent.childrenNodes[i + 1] = parent.childrenNodes[i];
			}
			parent.childrenNodes[splitIndex + 1] = right;

			for (int i = parent.numKeys - 1; i >= splitIndex; i--) {
				parent.keys[i + 1] = parent.keys[i];
			}
			parent.keys[splitIndex] = child.keys[t - 1];
			parent.numKeys++;

			//write(child)
			//write(right)
			//write(parent)
		}

		private void insertNonFull(BTreeNode currentNode, long akey) {
			int i = currentNode.numKeys - 1;//-1 for the index offset not given by the book

			if (currentNode.isLeaf == true) {
				while (i >= 0 && akey < currentNode.keys[i].key) {
					currentNode.keys[i + 1] = currentNode.keys[i];
					i--;
				}

				currentNode.keys[i + 1].key = akey;
				currentNode.numKeys++;
				//write(currentNode)

			} else {
				while (i >= 0 && akey < currentNode.keys[i].key) {
					i--;
				}
				i++;
				//read(currentNode.childrenNodes[i]
				BTreeNode child = childrenNodes[i];
				if (child.numKeys == maxNumKeys) {
					splitChild(currentNode, i);
					if (akey > currentNode.keys[i].key) {
						i++;
					}
				}
				insertNonFull(child, akey);

			}
		}

		public void insert(long akey) {
			//This first check implements the one-pass algorithm that splits any full nodes first before even inserting.
			//This avoids having to continuously split nodes as we progress up. Instead we are simply splitting the
			//nodes as we come to them so that they have vacancy before we even insert keys.

			//Is this node (first this node is the root) full?

			//CASE 1: insert on root as a leaf when root is full
			BTreeNode oldRoot = root;
			if (root.numKeys == maxNumKeys) {
				BTreeNode newRoot = new BTreeNode();
				root = newRoot;
				newRoot.isLeaf = false;
				newRoot.numKeys = 0;
				newRoot.childrenNodes[0] = oldRoot;
				splitChild(newRoot, 0);
				insertNonFull(newRoot, akey);
			} else {
				insertNonFull(oldRoot, akey);
			}
		}

		private Object[] clearArray(Object[] array) {
			for (int i = 0; i < array.length; i++) {
				array[i] = null;
			}
			return array;
		}
*/
	}


