public class ConverterTest_luke {
	public static void main(String [] args) {
		GeneParser_luke parser = new GeneParser_luke(3,"../data/test1.gbk");
		GeneConverter_luke conv = new GeneConverter_luke(3);

		int i = 0;
		while(!parser.isDone()) {
			System.out.println("Sequence " + i + ":");
			String result = parser.genSequence();
			System.out.println(result);
			System.out.println(Long.toBinaryString(conv.stringToKey(result)));
			i++;
		}
	}
}
