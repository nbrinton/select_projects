import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
/**
 * Created by nbrinton on 4/27/16.
 */
public class Test_BTreeRW {

	public static void main(String[] args) {
		int degree = Integer.parseInt(args[0]);
		String name = args[1];
		int seqLength = Integer.parseInt(args[2]);
		boolean debug = false;
		if(args[3] != null && Integer.parseInt(args[3]) == 1)
			debug = true;

		try{
		RandomAccessFile file = new RandomAccessFile("myBTree.gbk.btree.data."+seqLength+"."+degree,"rw");
		BTreeRW btree = new BTreeRW(degree,debug,file);
		GeneParser_nate parser = new GeneParser_nate(seqLength,name);
		GeneConverter_nate converter = new GeneConverter_nate(seqLength);
		while (parser.isDone() == false) {
			String seq = parser.genSequence();
			if(seq != null) { //checks if seq is null
				if(seq.length() == seqLength) { //makes sure only sequences of proper length inserted
					long key = converter.stringToKey(seq);
					btree.insert(key);
					System.out.println(btree);
				}
			}
		}
		System.out.println(btree);
		}catch(IOException e) {
			System.out.println("IOException occurred...");
		}

	}

}
