/**
 * Created by nbrinton on 4/16/16.
 */
import java.io.IOException;
import java.io.*;
import java.util.Random;
public class BTree {
	private int t;//degree
	private int maxNumKeys;
	private int maxNumChildren;
	BTreeNode_luke root;


	//Constructor
	public BTree(int degree) {
		this.t = degree;
		maxNumKeys = (2 * t) - 1;
		maxNumChildren = 2 * t;
		//possibly initialize root here. Or, initialize in a first case of insert()

	}

	public void insert(long akey) {
		root.insert(akey);
	}
	/**
	 * NOTE: make private before completion!!!
	 * Created by nbrinton on 4/16/16.
	 */
	public class BTreeNode_luke {
		private int numKeys;
		private int numChildren;
		private long offset;
		public BTreeObject_luke[] keys;//Array of BTreeObjects that tracks the keys and frequency of each key
		public long[] children;//Offset values
		private boolean isLeaf;
		private BTreeNode_luke childrenNodes[];//Direct descendant children BTreeNodes of this node

		//Constructor
		public BTreeNode_luke(int degree) {
			int maxNumKeys = (2 * degree) - 1;
			int maxNumChildren = 2 * degree;
			//initializing constants
			keys = new BTreeObject_luke[maxNumKeys];
			children = new long[maxNumChildren];
			childrenNodes = new BTreeNode_luke[maxNumChildren];

			//initializing variables
			this.offset = 0;
			numKeys = 0;
			numChildren = 0;
		}

		public void initBTOs() {
			Random rand = new Random();
			for(int i = 0; i < keys.length; i++) {
				keys[i] = new BTreeObject_luke(rand.nextLong());
			}
		}

		/**
		 * Set's the leaf status of this BTreeNode
		 *
		 * @param leafStatus
		 */
		public void setLeaf(boolean leafStatus) {
			this.isLeaf = leafStatus;
		}

		/**
		 * Sets the node's fileoffset
		 * @param offset
		 */
		public void setOffset(long offset) {
			this.offset = offset;
		}

		/**
		 * Writes this node to a binary file (RandomAccessFile)
		 * @param binaryFile file to write to
		 */
		public void write(RandomAccessFile binaryFile) {
			try {
			binaryFile.seek(offset); //seek to the file offset
			for(BTreeObject_luke bto: keys) //write BTreeObject keys
				bto.write(binaryFile);
			for(long l : children) //write children offsets
				binaryFile.writeLong(l);
			}catch(IOException e) {
				System.out.println("IOException occurred...");
			}
		}

		public void read(RandomAccessFile binaryFile,long offset) {
			try{
			binaryFile.seek(offset); //seek to the file offset
			for(BTreeObject_luke bto: keys) //write BTreeObject keys
				bto.read(binaryFile);
			for(long l : children) //write children offsets
				l = binaryFile.readLong();
			}catch(IOException e) {
				System.out.println("IOException occurred...");
			}
		}

		public void printNode() {
			for(BTreeObject_luke bto: keys) //write BTreeObject keys
				System.out.println(bto);
			for(long l : children) //write children offsets
				System.out.println(Long.toBinaryString(l));
		}

	}

}


