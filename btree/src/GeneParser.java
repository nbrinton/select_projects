/**
 * Created by nbrinton on 4/16/16.
 */

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class GeneParser {
	private File inputFile;
	private Scanner scan;
	private String input;
	private int seqLength;
	private int index;

	public GeneParser(int seqLength, String fileName) {
		this.input = "";
		this.index = 0;
		this.seqLength = seqLength;
		try {
			inputFile = new File(fileName);
			scan = new Scanner(inputFile);
			this.skipJunk();
		} catch (FileNotFoundException e) {
			System.out.println("Genebank file invalid");
		} catch (NullPointerException e) {
			System.out.println("File path is null");
		}
	}

	public void skipJunk() {
		String result = "";
		while (scan.hasNext() == true && result.equals("ORIGIN") == false) {
			result = scan.next().trim();
		}
	}

	public boolean isDone() {
		if (!scan.hasNext()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method generates the next sequence from the given gene file and calls the method genKey() to return the key
	 * needed to make a BTreeNode
	 */
	public String genSequence() {
		index++;

		//If there isn't anything else to read in, then return
		if (scan.hasNext() == false) {
			return null;
		}

		//While the input string is shorter than the necessary sequence length
		while (input.length() < seqLength) {
			String token = scan.next().trim();
			if (Character.isDigit(token.charAt(0))) {
				token = scan.next().trim();
			}
			/*
			//Ashley:
			if(input.contains("N")){
				input = input(0, N-2));
			}
			 */
			input += token;
		}
		String seq = input.substring(0, seqLength);
		seq = seq.toUpperCase();
		input = input.substring(1, input.length());
		//Does N mean the same as "//"?
		if (seq.contains("N") || seq.contains("/")) {
			return null;//Make sure to check for null case in CreateBTree
		} else {
			return seq;
		}
	}

	//For Testing
	public static void main(String[] args) {
		//Bad path test
		System.out.println("\n-------------------------------------------");
		System.out.println("BEGINNING: Bad filepath test");
		GeneParser badParser = new GeneParser(3, "./data/test1.gbk");
		System.out.println("\nFINISHING: Bad filepath test");
		System.out.println("-------------------------------------------\n");

		//Parser test
		System.out.println("\n-------------------------------------------");
		System.out.println("BEGINNING: Test Read-In Three Sequences");
		GeneParser parser = new GeneParser(7, "../data/test1.gbk");
//		String result0 = parser.genSequence();
//		String result1 = parser.genSequence();
//		String result2 = parser.genSequence();
//
//		System.out.println(result0);
//		System.out.println(result1);
//		System.out.println(result2);
		while (!parser.isDone()) {
			System.out.println(parser.genSequence());
		}
		System.out.println("\nFINISHING: Test Read-In Three Sequences");
		System.out.println("-------------------------------------------\n");

		System.out.println("TESTS COMPLETE");
	}
}
