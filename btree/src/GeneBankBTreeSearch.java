import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.Scanner;
import java.io.File;

/**
 * Created by nbrinton on 4/27/16.
 */
public class GeneBankBTreeSearch {

	public static void main(String[] args) {
		if(args.length != 2) {
			System.out.println("Usage: java GeneBankSearch <btree file> <query file> [<debug level>]");
			System.exit(1);
		}
		String treeName = args[0];
		String queryName = args[1];
		int debug = 0;
		if(args[2] != null && Integer.parseInt(args[2]) == 1)
			debug = 1;
		int seqLength = Integer.parseInt(treeName.substring(23,24));
		int degree = Integer.parseInt(treeName.substring(25,26));

		try{
		File afile = new File(queryName);
		Scanner scan = new Scanner(afile);
		RandomAccessFile file = new RandomAccessFile(treeName,"rw");
		BTree btree = new BTree(degree, debug, file);
		GeneConverter converter = new GeneConverter(seqLength);
		while (scan.hasNextLine()){
			String seq = scan.nextLine();
			if(seq != null) { //checks if seq is null
				if(seq.length() == seqLength) { //makes sure only sequences of proper length inserted
					long key = converter.stringToKey(seq);
					long result = btree.search(key);
					System.out.println("Key: "+converter.keyToString(key)+"Frequency: "+result);
				}
			}
		}
		System.out.println(btree);
		}catch(IOException e) {
			System.out.println("IOException occurred...");
		}

	}

}
//java GeneBankSearch <btree file> <query file> [<debug level>]
//java GeneBankCreateBTree <0/1(no/with Cache)> <degree> <gbk file> <sequence length>
//<cache size> [<debug level>]
