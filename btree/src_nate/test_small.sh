#!/bin/bash

# Compile
javac *.java

# Run first test and pipe the printed results to the files results.txt
# DEBUG OPTION: 1 for debug mode, ~1 for regular mode
java Test_BTree_nate ../data/small_gene_file_1.txt 1 >> small1.txt

vi small1.txt
