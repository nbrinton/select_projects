/**
 * Created by nbrinton on 4/16/16.
 */
public class BTree_cutchin {
	private int t;//degree
	private int maxNumKeys;
	private int maxNumChildren;
	private BTreeNode root;


	//Constructor
	public BTree_cutchin(int degree) {
		this.t = degree;
		maxNumKeys = (2 * t) - 1;
		maxNumChildren = 2 * t;
		//possibly initialize root here. Or, initialize in a first case of insert()

	}

	public void insert(long akey) {
		//if root is null, then create a new BTreeNode for the root (to keep in memory)
		root.insert(akey);
	}

	/**
	 * NOTE: make private before completion!!!
	 * Created by nbrinton on 4/16/16.
	 */
	public class BTreeNode {
		private int numKeys;
		private int numChildren;
		private BTreeObject_nate[] keys;//Array of BTreeObjects that tracks the keys and frequency of each key
		private long[] children;//Offset values
		private boolean isLeaf;
		private BTreeNode childrenNodes[];//Direct descendant children BTreeNodes of this node

		//Constructor
		public BTreeNode(/*long offset*/) {
			//initializing constants
			keys = new BTreeObject_nate[maxNumKeys];
			children = new long[maxNumChildren];
			childrenNodes = new BTreeNode[maxNumChildren];

			//initializing variables
			numKeys = 0;
			numChildren = 0;
		}

		/**
		 * Set's the leaf status of this BTreeNode
		 *
		 * @param leafStatus
		 */
		public void setLeaf(boolean leafStatus) {
			this.isLeaf = leafStatus;
		}

		public void write() {

		}

		public void read() {

		}

		public void insert(long akey) {
			//This first check implements the one-pass algorithm that splits any full nodes first before even inserting.
			//This avoids having to continuously split nodes as we progress up. Instead we are simply splitting the
			//nodes as we come to them so that they have vacancy before we even insert keys.

			//Is this node (first this node is the root) full?

			//CASE 1: insert on root as a leaf when root is full
			if (numKeys == maxNumKeys) {
				//split
				BTreeNode left = new BTreeNode();
				BTreeNode right = new BTreeNode();
				//left.write();
				//right.write();
				int m = numKeys / 2;
				for (int i = 0; i < m; i++) {
					left.keys[i] = this.keys[i];
					left.children[i] = this.children[i];
					left.numKeys = m;
				}

				for (int i = m + 1; i < numKeys; i++) {
					right.keys[i - (m + 1)] = this.keys[i];
					right.children[i - (m + 1)] = this.children[i];
					right.numKeys = m;
				}

				//left.write();
				//right.write();

				//Update this node
				this.isLeaf = false;//probably have this where I create the root node
				this.numKeys = 1;
				BTreeObject_nate pushedUpKey = this.keys[m];
				//clear the current node
				clearArray(keys);
				//re-insert the pushed-up key into the first index of this node
				this.keys[0] = this.keys[m];
				clearArray(childrenNodes);
//			    this.children[0] = left.offset
				this.childrenNodes[0] = left;
//		    	this.children[1] = right.offset
				this.childrenNodes[1] = right;
			}

			//Is this node a leaf?
			if (this.isLeaf == true) {
				//is this node full?
				if (this.numKeys == maxNumKeys) {
					//split (leaf-case)
					//move the key at m into the appropriate location in the parent node
					int m = numKeys / 2;
					if (akey < keys[m].key) {
						BTreeNode left = new BTreeNode();
						for (int i = 0; i < m; i++) {
							left.keys[i] = this.keys[i];
							left.children[i] = this.children[i];
							left.numKeys = m;
						}
					}

					BTreeNode right = new BTreeNode();


					for (int i = m + 1; i < numKeys; i++) {
						right.keys[i - (m + 1)] = this.keys[i];
						right.children[i - (m + 1)] = this.children[i];
						right.numKeys = m;
					}
					//move all of the keys left of m into a new left node
					//move all of the keys right of m into a new right node
				}
				//if full, split (leaf split is slightly different)

				//else check for duplicates and insert the key or increment freqCount
				//If akey is larger than the far-right key
				if (akey > keys[numKeys - 1].key) {
					keys[numKeys].key = akey;
				} else {
					//Iterate through the keys array until we find a key that is larger than akey
					for (int i = 0; i < numKeys; i++) {
						if (akey < keys[i].key) {
							//shift the keys over
							for (int j = maxNumKeys - 1; j > i; j--) {
								keys[j] = keys[j - 1];
							}
							keys[i].key = akey;
						} else if (akey == keys[i].key) {
							keys[i].incrementFrequency();
						}
					}
				}

				return;//exit, we are done inserting for case 2: insert on a leaf


			} else {
				//Case: internal node

//	    		int childIndex = findChild(akey);
				int childIndex = 0;
				for (int i = 0; i < numKeys; i++) {
					if (keys[i].key == akey) {
						childIndex = i;
					}
				}
//				BTreeNode achild = BTreeNode.readNode(fileOffset);
				BTreeNode achild = childrenNodes[childIndex];

				if (achild.numKeys == maxNumKeys) {
					//split
					BTreeNode right = new BTreeNode();
					//right.write();
					int m = achild.numKeys / 2;
					for (int i = childIndex; i < numKeys; i++) {

					}
				}
//
//			if(achild.numKeys == maxNumKeys){
//			    //split
//			    BTreeNode right = new BTreeNode(t);
//			    right.write();
//			    //move into this & adjust
//			    //insert right in this.child[childIndex + 1];
//			    //move Keys from achild to right
//			    //truncate size of achild
//
//	        	if(akey > m){
//		    	    right.insert(akey);
//		        }else if(akey < m){
//			        achild.insert(akey);
//              }else{
//			        increase freq //handles internal node
			}
		}

		private Object[] clearArray(Object[] array) {
			for (int i = 0; i < array.length; i++) {
				array[i] = null;
			}
			return array;
		}

	}

}
