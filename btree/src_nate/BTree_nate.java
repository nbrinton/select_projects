import java.io.RandomAccessFile;
import java.io.IOException;
import java.io.File;
import java.io.PrintWriter;

/**
 * Created by nbrinton on 4/16/16.
 */
public class BTree_nate {
	private int t;//degree
	private int maxNumKeys;
	private int maxNumChildren;
	private BTreeNode_nate root;
	private RandomAccessFile binaryFile;
	private long writePointer; //keeps track of index to write to file at
	private int debug;
	private int seqLength;
	private File dump;
//	private PrintWriter writer;
//	private GeneConverter geneConverter;


	//Constructor
	public BTree_nate(int degree, int seqLength, int debug) {
		this.debug = debug;
		this.t = degree;
		maxNumKeys = (2 * t) - 1;
		maxNumChildren = 2 * t;
		//possibly initialize root here. Or, initialize in a first case of insert()

//		if (debug == 1) {
//			dump = new File("./dump");
//			dump.createNewFile();
//			writer = new PrintWriter(dump);
//			geneConverter = new GeneConverter(seqLength);
//		}

	}

	public void insert(long akey) {
		//if root is null, then create a new BTreeNode_nate for the root (to keep in memory)
		if (root == null) {
			root = new BTreeNode_nate();
		}
		root.insert(akey);
	}

	/**
	 * NOTE: make private before completion!!!
	 * Created by nbrinton on 4/16/16.
	 */
	public class BTreeNode_nate {
		private BTreeObject_nate[] keys;//Array of BTreeObject_nates that tracks the keys and frequency of each key
		private long[] children;//Offset values
		private BTreeNode_nate childrenNodes[];//Direct descendant children BTreeNode_nates of this node

		//Constructor
		public BTreeNode_nate(/*long offset*/) {
			//initializing constants
			keys = new BTreeObject_nate[maxNumKeys];
			children = new long[maxNumChildren];
			childrenNodes = new BTreeNode_nate[maxNumChildren];
		}

		public boolean isLeaf() {
			if (childrenNodes[0] != null)
				return false;
			else
				return true;
		}

		public int getNumKeys() {
			int numKeys = 0;
			for (int i = 0; i < keys.length; i++) {
				if (this.keys[i] != null)
					numKeys++;
			}
			return numKeys;
		}

		public int getNumChildren() {
			int numChildren = 0;
			for (int i = 0; i < keys.length; i++) {
				if (this.childrenNodes[i] != null)
					numChildren++;
			}
			return numChildren;
		}

		public String toString(int height, int childNum) {
			String str = "";
			if (this == root) {
				str += "root\n";
			} else {
				str += "|";
				String indent = "";
				for (int i = 0; i < height; i++) {
					indent += "-";
				}
				str += indent + "> c (" + height + ", " + childNum + ") ";
			}
			str += "[ ";
			childNum = 0;
			for (int i = 0; i < this.getNumKeys(); i++) {
				str += this.keys[i] + ", ";
			}
			str += " ]\n";

			childNum = 0;
			for (int i = 0; i < this.childrenNodes.length; i++) {
				childNum++;
				if (childrenNodes[i] != null) {
					str += childrenNodes[i].toString((height + 1), (childNum));
				} else {
					height = 0;
				}
			}
			return str;
		}

		/**
		 *
		 */
//		public void writeNodeDump() {
//			for (int i = 0; i < this.getNumKeys(); i++) {
//				writer.print(geneConverter.keyToString(this.keys[i].key) + " " + this.keys[i].key.freq);
//			}
//			for (int i = 0; i < this.childrenNodes.length; i++) {
//				if (this.childrenNodes[i] != null) {
//					this.childrenNodes[i].writeNodeNump();
//				}
//			}
//		}

		/**
		 * Writes this node to a binary file (RandomAccessFile)
		 *
		 * @param binaryFile file to write to
		 */
	/*
		public void write(RandomAccessFile binaryFile) {
			try {
			binaryFile.seek(offset);
			for(BTreeObject bto: keys) //write BTreeObject keys
				bto.write(binaryFile);
			for(long l : children) //write children offsets
				binaryFile.writeLong(l);
			}catch(IOException e) {
				System.out.println("IOException occurred...");
			}
		}

		public void read(RandomAccessFile binaryFile,long offset) {
			try{
			binaryFile.seek(offset); //seek to the file offset
			for(BTreeObject bto: keys) //write BTreeObject keys
				bto.read(binaryFile);
			for(long l : children) //write children offsets
				l = binaryFile.readLong();
			}catch(IOException e) {
				System.out.println("IOException occurred...");
			}
		}
		*/
		private void splitChild(BTreeNode_nate parent, int splitIndex) {
			if (debug == 0) {
				System.out.println("Entered splitChild( )");
			}
			BTreeNode_nate right = new BTreeNode_nate();
			BTreeNode_nate child = parent.childrenNodes[splitIndex];

			for (int i = 0; i < t - 1; i++) {
				right.keys[i] = new BTreeObject_nate(child.keys[i + t].key);
			}

			if (child.isLeaf() == false) {
				for (int i = 0; i < t; i++) {
					right.childrenNodes[i] = child.childrenNodes[i + t];
				}
			}

			for (int i = parent.getNumKeys(); i > splitIndex; i--) { //was splitIndex + 1
				parent.childrenNodes[i + 1] = parent.childrenNodes[i];
			}
			parent.childrenNodes[splitIndex + 1] = right;

			for (int i = parent.getNumKeys() - 1; i >= splitIndex; i--) {
				parent.keys[i + 1] = parent.keys[i];
			}
			parent.keys[splitIndex] = child.keys[t - 1];

			//Clear the old values from the right of the child node
			for (int i = t; i < maxNumChildren; i++) {
				child.childrenNodes[i] = null;
			}
			for (int i = t - 1; i < maxNumKeys; i++) {
				child.keys[i] = null;
			}

			//write(child)
			//write(right)
			//write(parent)
		}

		private void insertNonFull(BTreeNode_nate currentNode, long akey) {
			if (debug == 0) {
				System.out.println("Entered insertNonFull( )");
				System.out.println("# keys = " + currentNode.getNumKeys());
			}
			int i = currentNode.getNumKeys() - 1;//-1 for the index offset not given by the book

			if (currentNode.isLeaf() == true) {
				if (debug == 0) {
					System.out.println("Node is Leaf:");
				}
				if (i >= 0 && akey < currentNode.keys[i].key) {
					if (debug == 0) {
						System.out.println("Shifting keys to the right...");
					}
				}

				/*
				if (i > -1 && currentNode.keys[i].key == akey) {
					currentNode.keys[i].incrementFrequency();
					return;
				}
				*/
				if (i > -1 && currentNode.contains(akey)) {
					currentNode.keys[currentNode.indexOf(akey)].incrementFrequency();
					return;
				}

				while (i >= 0 && akey < currentNode.keys[i].key) {
					currentNode.keys[i + 1] = currentNode.keys[i];
					i--;
				}

				currentNode.keys[i + 1] = new BTreeObject_nate(akey);
				if (debug == 0) {
					System.out.println("KEY INSERTED: " + akey);
				}

				//write(currentNode)

			} else {
				if (i > -1 && currentNode.contains(akey)) {
					currentNode.keys[currentNode.indexOf(akey)].incrementFrequency();
					return;
				}

				if (debug == 0) {
					System.out.println("Node NOT Leaf:");
				}
				if (i >= 0 && akey < currentNode.keys[i].key) {
					if (debug == 0) {
						System.out.println("Shifting the index. (i = " + i + ")");
					}
				}

				while (i >= 0 && akey < currentNode.keys[i].key) {
					i--;
				}

				if (debug == 0) {
					System.out.println("(i = " + i + ")");
				}

				i++;

				if (debug == 0) {
					System.out.println("Increment i: \n(i = " + i + ")");
				}
				//read(currentNode.childrenNodes[i]

				BTreeNode_nate child = currentNode.childrenNodes[i];
				if (child.getNumKeys() == maxNumKeys) {
					splitChild(currentNode, i);
					i = 0; //added this to look at all keys to find right child.
					while (currentNode.keys[i] != null && akey > currentNode.keys[i].key) { //changed from if
						i++;
						if (i >= childrenNodes.length - 1)
							break;
					}
					child = currentNode.childrenNodes[i];
				}
				insertNonFull(child, akey);

			}
		}

		public void insert(long akey) {
			if (debug == 0) {
				System.out.println("INSERTING: " + akey);
			}

			//CASE 1: insert on root as a leaf when root is full
			BTreeNode_nate oldRoot = root;
			if (root.getNumKeys() == maxNumKeys) {
				if (debug == 0) {
					System.out.println("Node is full: ");
				}
				BTreeNode_nate newRoot = new BTreeNode_nate();
				root = newRoot;
				newRoot.childrenNodes[0] = oldRoot;
				splitChild(newRoot, 0);
				insertNonFull(newRoot, akey);
			} else {
				insertNonFull(oldRoot, akey);
			}

		}

		public boolean contains(long akey) {
			for (int i = 0; i < this.keys.length; i++) {
				if (this.keys[i] != null && this.keys[i].contains(akey))
					return true;
			}
			return false;
		}

		public int indexOf(long akey) {
			for (int i = 0; i < this.keys.length; i++) {
				if (this.keys[i] != null && this.keys[i].contains(akey))
					return i;
			}
			return -1;
		}


	}

	public String toString() {
		String str = "";
		str += root.toString(0, 0);
		return str;
	}

//	public void writeDump() {
//		root.writeNodeDump();
//	}

}
