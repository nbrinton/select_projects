/**
 * @author lbosse
 *         04/19/2016
 */

public class GeneParserTest_nate {
	public static void main(String[] args) {
		//Bad path test
		System.out.println("Bad filepath test");
		GeneParser_nate badParser = new GeneParser_nate(3, "./data/test1.gbk");

		//Parser test
		GeneParser_nate parser = new GeneParser_nate(3, "../data/test1.gbk");
		String result0 = parser.genSequence();
		String result1 = parser.genSequence();
		String result2 = parser.genSequence();

		System.out.println(result0);
		System.out.println(result1);
		System.out.println(result2);

		System.out.println("test complete");
	}
}
