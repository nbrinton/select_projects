#!/bin/bash

# Compile
javac *.java

# Run first test and pipe the printed results to the files results.txt
# DEBUG OPTION: 1 for debug mode, ~1 for regular mode
java Test_BTree_nate ../data/test3.gbk 1 >> results3.txt

vi results3.txt
