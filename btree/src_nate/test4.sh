#!/bin/bash

# Compile
javac *.java

# Run first test and pipe the printed results to the files results.txt
# DEBUG OPTION: 1 for debug mode, ~1 for regular mode
java Test_BTree_nate ../data/test4.gbk 1 >> results4.txt

vi results4.txt
