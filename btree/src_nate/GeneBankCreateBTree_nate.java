
/**
 * Created by nbrinton on 4/16/16.
 */
public class GeneBankCreateBTree_nate {


	public static void main(String args[]) {

		if (args.length != 4) {
			System.err.println("USAGE:\t<degree> <gbk file> <sequence length> [<debug level>]\n");
			System.err.println("\t<degree> : the the degree to be used for the btree. Selecting 0 defaults to optimized degree for disk block size 4096 bytes.");
			System.err.println("\t<gbk file> : the gene bank file to be read-in by the btree");
			System.err.println("\t<sequence length> : the specified length of each sequence to be read in from the gbk file");
			System.err.println("\t[<debug level>] : debug options");
			System.err.println("\t\t0 : Any diagnostic, help, and status messages printed to standard error stream");
			System.err.println("\t\t1 : Output of <frequency> <DNA string> for each stored key is printed to a text file named \"dump\"");
			System.exit(0);
		}

		int t = Integer.parseInt(args[0]);
		String filename = args[1];
		int seqLength = Integer.parseInt(args[2]);
		int debug = 0;//0
		if (Integer.parseInt(args[3]) == 1) {
			debug = 1;
		}

		BTree_nate btree = new BTree_nate(t, seqLength, debug);
		GeneParser_nate parser = new GeneParser_nate(seqLength, filename);
		GeneConverter_nate converter = new GeneConverter_nate(seqLength);

		if (debug == 1) {

		}

		while (parser.isDone() == false) {
			String sequence = parser.genSequence();
			if (sequence != null) {
				if (sequence.length() == seqLength) {
					long key = converter.stringToKey(sequence);
					btree.insert(key);
					if (debug == 0) {
						System.out.println(btree);
					}

				}
			}
		}
//		if (debug == 1) {
//			btree.writeDump();
//		}

		System.out.println("\n\nFINAL BTREE:\n");
		System.out.println(btree);


	}

}
