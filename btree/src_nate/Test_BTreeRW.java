import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
/**
 * Created by nbrinton on 4/27/16.
 */
public class Test_BTreeRW {

	public static void main(String[] args) {
		String name = args[0];
		boolean debug = false;
		if(args[1] != null && Integer.parseInt(args[1]) == 1){
			debug = true;
		}
		try{
		RandomAccessFile file = new RandomAccessFile("test.bin","rw");
		BTreeRW btree = new BTreeRW(2, debug,file);
		GeneParser_nate parser = new GeneParser_nate(3, name);
		GeneConverter_nate converter = new GeneConverter_nate(3);
		while (parser.isDone() == false) {
			System.out.println("entered parser loop");
			String seq = parser.genSequence();
			if(seq != null) { //checks if seq is null
				if(seq.length() == 3) { //makes sure only sequences of proper length inserted
					long key = converter.stringToKey(seq);
					btree.insert(key);
					System.out.println(btree);
				}
			}
		}
		System.out.println(btree);
		}catch(IOException e) {
			System.out.println("IOException occurred...");
		}

	}

}
