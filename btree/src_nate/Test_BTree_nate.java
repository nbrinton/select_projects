/**
 * Created by nbrinton on 4/27/16.
 */
public class Test_BTree_nate {

	public static void main(String[] args) {
		String name = args[0];
		int debug = 0;
		if(args[1] != null && Integer.parseInt(args[1]) == 1){
			debug = 1;
		}
		BTree_nate btree = new BTree_nate(2, 3, debug);
		GeneParser_nate parser = new GeneParser_nate(3, name);
		GeneConverter_nate converter = new GeneConverter_nate(3);
		while (parser.isDone() == false) {
			String seq = parser.genSequence();
			if(seq != null) { //checks if seq is null
				if(seq.length() == 3) { //makes sure only sequences of proper length inserted
					long key = converter.stringToKey(seq);
					btree.insert(key);
					System.out.println(btree);
				}
			}
		}
//		System.out.println(btree);

	}

}
