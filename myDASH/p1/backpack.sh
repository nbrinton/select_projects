#!/bin/bash
if [ "$1" = "" ];then
  echo "usage: $0 <output file>"
  echo "   output file - the file to save output in"
  exit 0;
fi

inside="../../$1"
dest="../$1"
home="$1"

path="tests/"
shell="../../mydash"

#Generate the students assignment
#Make should return no errors
make
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - make returned non-zero" 
	exit 1
fi

#Make sure that there is an exe named mydash at top level
if [ ! -x "mydash" ];then
	echo "P1-ck: FAIL - no exe named mydash at top level"
	exit 1
fi

#Make sure that they have all the libs
if [ ! -e "libmylib.so" ];then
	echo "P1-ck: FAIL - libmylib.so missing (your linked list)"
	exit 1
fi


echo "START: Testing the empty command" >> $home
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
cd $path
cd test-empty
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
cat input | $shell
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - empty command" >> $dest
	exit 1
fi
cd ../
echo "  END: Testing the empty command" >> $dest


echo "START: Testing EOF" >> $dest
cd test-eof
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
cat input | $shell
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - EOF" >> $dest
	exit 1
fi
cd ../
echo "  END: Testing EOF" >> $dest


echo "START: Testing exit" >> $dest
cd test-exit
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
cat input | $shell
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - exit command" >> $inside
	exit 1
fi
cd ../
echo "  END: Testing exit" >> $dest


echo "START: Testing cd" >> $dest
cd test-cd
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
cat input | $shell
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - exit return code from mydash" >> $inside
	exit 1
fi
if [ ! -e ../"___CD-TEST___" ];then
	echo "P1-ck: FAIL - ACK!!! Tried to create a file named ___CD-TEST___ and we don't know where it went!" >> $inside
	exit 1
else
	rm ../"___CD-TEST___"
fi
cd ../
echo "  END: Testing cd " >> $dest


echo "START: Testing version" >> $dest
cd ../
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./
./mydash -v
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - version command did not exit with non-zero" >> $inside
	exit 1
fi
cd tests/
echo "  END: Testing version" >> $dest


echo "START: Testing ls" >> $dest
cd test-ls
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
cat input  | $shell > actual
ls > expected
sed -i '/mydash>/d' ./actual
sed -i '/actual/d' ./actual
sed -i '/actual/d' ./expected
sed -i '/expected/d' ./actual
sed -i '/expected/d' ./expected
diff actual expected
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - ls command output did not match that of bash" >> $inside
	exit 1
fi
rm -rf actual
rm -rf expected
cd ../
echo "  End: Testing ls" >> $dest


echo "START: Testing ls with flags" >> $dest
cd test-ls-flags
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
cat input | $shell > actual
ls -a > expected
sed -i '/mydash>/d' ./actual
sed -i '/actual/d' ./actual
sed -i '/actual/d' ./expected
sed -i '/expected/d' ./actual
sed -i '/expected/d' ./expected
diff actual expected
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - ls command with flags output did not match that of bash" >> $inside
	exit 1
fi
rm -rf actual
rm -rf expected
cd ../
echo "  End: Testing ls with flags" >> $dest


echo "START: Testing cd with no args" >> $dest
cd test-cd-no-args
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
cat input | $shell
cd
if [ ! -e ./"made-it-home" ];then
	cd -
	echo "P1-ck: FAIL - ACK!!! cd with no arguments did not take us to the home directory!" >> $inside
	exit 1
else
	rm ./"made-it-home"
	cd -
fi
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - cd command with no args did not exit with non-zero" >> $inside
	exit 1
fi
cd ../
echo "  End: Testing cd with no args" >> $dest


#echo "START: Testing customizeable prompt" >> $dest
#DASH_PROMPT="custom_prompt> "
#export DASH_PROMPT
#make clean
#make
#echo "custom_prompt> exit" > custom-prompt-input
#cat test-custom-prompt |./mydash
#diff custom-prompt-input custom-prompt-output
#if [ ! $? -eq 0 ];then
#	echo "P1-ck: FAIL - custom prompt was not set correctly" >> $dest
#	exit 1
#fi
#rm -rf custom-prompt-input
#rm -rf custom-prompt-output
#DASH_PROMPT="mydash> "
#export DASH_PROMPT
#make
#echo "  End: Testing customizeable prompt" >> $dest

echo "START: Testing jobs with no background jobs running" >> $dest
cd test-jobs-with-no-background-jobs
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
cat input | $shell > actual
sed -i '1d' actual
echo "" >> actual
diff actual expected
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - The jobs command does prints something besides the prompt even though there are currently no background jobs" >> $inside
	exit 1
fi
rm -rf actual
cd ../
echo "  End: Testing jobs with no background jobs running" >> $dest


#echo "START: Testing against Valgrind" >> $dest
#cd ../
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./
#valgrind --leak-check=full ./mydash
#if [ ! $? -eq 0 ];then
#	echo "P1-ck: FAIL - The mydash shell does not pass valgrind!" >> $home
#	exit 1
#fi
#echo "  End: Testing against Valgrind" >> $home


#echo "START: Testing 2048 arguments" >> $home
#i="0"
#ls_args=" "
#while [ $i -lt 2048 ]
#do
#	i=$[$i+1]
#	touch file"$i"
#	ls_args+="file$i"
#done
#ls $ls_args
#if [ ! $? -eq 0 ];then
#	echo "P1-ck: FAIL - The ls command could not handle 2048 arguments" >> $home
#	exit 1
#fi
#while [ $i -lt 2048 ]
#do
#	i=$[$i+1]
#	rm -f file"$i"
#done

echo "START: Testing 2048 arguments" >> $dest
cd test-2048-arguments
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../
i="0"
#ls_args=" "
echo -n "ls " >> input
while [ $i -lt 2046 ]
do
	i=$[$i+1]
	touch file"$i"
#	ls_args+="file$i "
	echo -n "file$i " >> input
done
#ls $ls_args
cat input | $shell
if [ ! $? -eq 0 ];then
	echo "P1-ck: FAIL - The ls command could not handle 2048 arguments" >> $inside
	exit 1
fi
i="0"
while [ $i -lt 2046 ]
do
	i=$[$i+1]
	rm -f file"$i"
done
rm -rf input
cd ../
echo "  End: Testing 2048 arguments" >> $dest


#echo "Valgrind was NOT run, make sure and test with valgrind" >> $dest
cd ../
echo "PASSED: All smoke tests passed!" >> $home
cat $home
