#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
make clean
make
clear
valgrind --leak-check=yes --suppressions=valgrind.supp ./mydash
