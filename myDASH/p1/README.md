#Project 1: Shell Project Part 1 {#mainpage}

* Author: Nate Brinton
* Class: CS453 [Operating Systems] Section 002

##Overview

This program is a simple shell program called "mydash" (My Dead Again SHell).
The shell has built in commands "exit", "cd", and "jobs", and can also 
execute other already-available commands via forking and execvp. The shell
script "backpack.sh" runs a list of tests (located in the tests directory) on
"mydash" and prints the results. For ease of development, I also created two
simple shell scripts "run" and "run-eval" that run both with and without 
valgrind accordingly. MyDASH also utilizes a linked-list data structure library
that I wrote to store the jobs. The source code for the linked-list library is
located back one directory in "p0".


##Manifest

./backpack.sh -- A shell script that runs tests on mydash
./doxygen-config -- A doxygen configuration file
./Makefile -- The top level makefile
./p1-rubric.txt -- A grading rubric for this project
./README.md -- This document
./run.sh -- A simple shell script that sets the export path and runs mydash
./run-val.sh -- Same as run but also runs on valgrind with the suppression file
./valgrind.supp -- A supression file for valgrind to ignore readline errors

./docs/ -- A directory generated using doxygen that contains documentation
./tests/ -- A directory containing the input and expected files for each test

./mydash-src/job.c -- A .c file that represents a job struct and its functions
./mydash-src/job.h -- The header file for job.c
./mydash-src/mydash.c -- The main source code file for mydash
./mydash-src/mydash.h -- The header file for mydash.c
./mydash.c/Makefile -- The inner-level makefile


##Building the project

In order to use mydash, you must have an installed linked-list library that you
can link to. Currently, mydash links to my library. If you choose to use a 
different library, just see that you modify this path accordingly. In addition 
to a linked-list library, you need to ensure that your system has the ncurses
and readline libraries installed. The documentation for these libraries will be
a better guide for how to instal them on your system, but for debian-based linux,
you can install the necessary packages using the commands below:
	sudo apt-get install libncurses5-dev 
	sudo apt-get install libreadline6 libreadline6-dev

To build the program, first set the path to find the library by entering: 
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./
Then, run the makefile:
	make
Now you should see the executable for mydash. Run it by entering:
	./mydash
If you simply wish to see the version of mydash, enter:
	./mydash -v
and the program will print out the version and exit.

From here, mydash should operate like a simplified version of bash. Go ahead and 
try out some commands.

In addition to performing these steps manually, you can run one of two shell 
scripts that do this for you. However, you will most likely have to set these
two scripts as executable files by performing the following:
	chmod +x run.sh
	chmod +x run-val.sh

To run myDASH easily, enter:
	./run.sh

Should you choose to test the program using valgrind, then you can use 
run-valgrind.sh which also exports the path, makes the project, and then runs
myDASH in valgrind. You can do this by entering:
	./run-val.sh


##Features and usage

The mydash shell program comes with three built-in commands: exit, cd, and jobs. 

The jobs comand can be performed by entering:
	jobs
The jobs command will print out any processes currently running in the 
background. To make a progress run in the background, simply append an ampersand 
symbol (&) to the end of it, either with or without a space). Once all of the 
jobs running in the background have completed, the list of jobs will print and 
then reset once the user enters anything. I originally had it only refresh the 
list if the user only hits enter, but this can be changed back easily. I changed
it to the way it works now in order to more closely mimick bash.

The exit command will exit the mydash shell and return you to the terminal you
ran it on. To use the exit command, simply enter:
	exit

The cd command is used to Change Directories. To use it, enter:
	cd <filepath>
to change into the directory at the given filepath. If no filepath is given, the 
command automatically changes into the home directory (~/).

Most other non-built-in commands that you can run on bash you should be able to
run on mydash such as the "ls" command etc. since they are just small programs
and not directly part of the shell program.


##Testing

While I was developing mydash, I would always try to break it by trying various
combinations of commands, incorrect commands, nonexistent commands, etc. I'm 
still working on more tests in the backpack.sh script, but I'm unsure how to 
write some of the tests appropriately since some of them such as ones with jobs
have an asynchronous and non-replicable manner (different job ID's making it
difficult to write an "expected" file for instance). Some of the tests in 
backpack.sh include testing EOF, the "empty" test (user just hitting enter), 
the exit command, the ls command, and a few more. Despite the lack of written 
tests for the jobs command, I ran many different combinations of the jobs command
on my own. For instance, entering three sleep commands in the background and 
hitting enter until finally they finish and print then entering sleep again and 
seeing if the joblist reset as it was supposed to. This is just an example of one
such tests I have tried on the jobs command. I currently hace commented-out test
for the customizeable prompt because it was having some issues, however, I tested
this out also by hand and it worked. If I had more time, I would like to write 
more tests to be thorough.

I also more recently had to go back in and adjust a few things and add some more
clarity to this readme as I realized that after cloning the repo and looking at
things on a different system, I had previously made too many assumptions regarding
some of the project dependencies. I added some more documentation to clarify a 
few setup steps as well as ensure that the user has the correct dependencies to
run the program.


###Valgrind

I tried to run my program through valgrind as often as possible in order to stop
any memory leaks right as they happend so they were easier to trace back to the 
root of the problem. This is why I created the run-val shell script to do this 
easily. Currently, I don't appear to have any memory leaks when running valgrind!
I was having one that would happen when I created background jobs, but that was 
fixed by freeing a node that I had created in my updateJobs function.


###Known Bugs

I don't currently know of any bugs with my mydash program, but I do know that my
testing suite needs to be more thorough. I'm also still not extremely comfortable
with knowing the best way to structure C programs for ultimate efficiency and 
readability/organization, so that could probably use some improvement I'm sure.


##Discussion

This project was a massive undertaking for me, and I'm so glad that I shadowed
Marissa's CS253 section last semester in order to refresh and strengthen my
knowledege of C because I would have been absolutely overwhelmed had I not. This
project was certainly good practice in both looking up, reading, and atttempting
to understand the documentation of functions such as execvp() etc. 

I understood, for the most part, fairly well how to tackle the built-in commands,
but the process handling was and still is fairly obscure to me, and I had to get
some help from both Trevor and Sam regarding the functionality of waitpid and the
WNOHANG argument.

Surpisingly, probably one of the biggest issues I ran into was just trying to do
string manipulation in C. Being used to all of Java's nice readily available 
commands certainly made it challenging to write my code and think 
algorithmically without them. 


##Sources used

For help with my job (process) status handling, I used and some of the code I
found here regarding the return values of waitpid and what they meant.

[Page I found on Stack Overflow (I know...)](http://stackoverflow.com/questions/4200373/just-check-status-process-in-c)

I also used this source to find out how to enable the colors on the ls command.
[ls colors](https://www.cyberciti.biz/faq/how-to-turn-on-or-off-colors-in-bash/)

Used this website as a reference for shell script while-loop syntax
[bash while-loop syntax](https://linux.die.net/Bash-Beginners-Guide/sect_09_02.html)

