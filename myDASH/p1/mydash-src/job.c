#include "job.h"
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


struct job* createJob(int index, pid_t id, const char *command) {
	struct job* newJob = (struct job*) malloc (sizeof(struct job));
	newJob->index = index;
	newJob->id = id;
	newJob->command = (char *) malloc(sizeof(char)*(strlen(command)+1));
	strcpy(newJob->command, command);
	newJob->status = 1;//running
	return newJob;
}

int equals(const void *job1, const void *job2) {
	struct job* j1 = (struct job*) job1;
	struct job* j2 = (struct job*) job2;
	return j1->id == j2->id;
}

char *jobStart(const void *job) {
	struct job* myjob = (struct job*) job;

	// calculate and allocate enough space for the output string.
	int max_command_len = strlen(myjob->command)+1;
	max_command_len += MAX_KEY_DIGITS;//max number of digits for the index
	max_command_len += 4; // include [] and space
	char *temp = (char *) malloc(sizeof(char) * max_command_len);

	//print into our buffer safely
	snprintf(temp, max_command_len, "[%d] %d\t%s", myjob->index, myjob->id, myjob->command);
	return temp;
}

char *toString(const void *job) {
	struct job* myjob = (struct job*) job;
	
	char *msg;
	if (myjob->status == 0) {
		msg = "Done   ";
	} else {
		msg = "Running";
	}

	// calculate and allocate enough space for the output string.
	int max_command_len = strlen(myjob->command)+1;
	max_command_len += MAX_KEY_DIGITS;//max number of digits for the index
	max_command_len += 4; // include [] and space
	max_command_len += strlen(msg);//Length of "Running "
	char *temp = (char *) malloc(sizeof(char) * max_command_len);

	//print into our buffer safely
	snprintf(temp, max_command_len, "[%d] %s %d\t%s", myjob->index, msg, myjob->id, myjob->command);
	return temp;
}

void freeJob(void *job) {
	struct job* myjob = (struct job*) job;
	if (myjob == NULL) return;
	free(myjob->command);
	free(myjob);
}

int updateJob(void *job) {
	struct job *myjob = (struct job*) job;
	int status;
	pid_t pid = waitpid(myjob->id, &status, WNOHANG);
	if (pid == -1) {	// waitpid error
//		fprintf(stderr, "what is this error and why does it happen when I uncomment this out?!?");
		return -1;
	} else if (pid == 0) {	// This job's process is still running
		myjob->status = 1;
		return 1;
	} else if (pid == myjob->id) {	// This job's process has terminated
		myjob->status = 0;
		return 0;
	}
	return 0;
}
