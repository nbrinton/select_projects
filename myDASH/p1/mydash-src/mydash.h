#ifndef	__MYDASH_H
#define	__MYDASH_H

#define	MAXLINE	4096			/* max line length */

#define	FILE_MODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

#include	<sys/types.h>			// Required for pid_t etc types
#include	<sys/wait.h>			// Required to use the wait function
#include	"mydash.h"
#include	<sys/types.h>			// Required for some of our prototypes for convenience
#include	<stdio.h>
#include	<stdlib.h>			
#include	<sysexits.h>			// Required for using the exit function
#include	<string.h>				// Required for common string functions
#include	<unistd.h>				// Required for pid_t etc types
#include	<ncurses.h>				// Required for using ncurses library function calls?
#include	<readline/readline.h>	// Required for readline
#include	<readline/history.h>	// Required for readline
#include	<pwd.h>					// Required for cd command functionality
#include	<ctype.h>				// Required for isspace method used in white()
#include "../../p0/libsrc/List.h"	// Required for using my linked-list
#include "../../p0/libsrc/Node.h"	// Required for using my linked-list
#include	<job.h>					// Required for using my job struct

/**
 * Returns an array of tokens for each argument in the given command string
 *
 * @param str The command string
 * @param delimiter The delimiter character to tokenize on
 * @param numTokens A pointer to an integer to store the number of tokens in the returned array of tokens
 * @return tokens the array of tokenized commands
 */
char **parseInput(char *str, char *delimiter, int *numTokens);

/**
 * Returns TRUE if the line only contains whitespace, and FALSE otherwise
 *
 * @param line The line to check 
 * @return TRUE (1) or FALSE (0)
 */
int white(char *line);

/**
 * Returns the version of this shell (created in our makefile)
 *
 * @return 
 */
const char* git_version(void);

/**
 * Returns TRUE if the job needs to be run in the background 
 * 
 * @param args The array of commands
 * @param numTokens The number of tokens in args
 * @return TRUE (1) or FALSE (0)
 */
int isBackground(char **args, int numTokens);

/**
 * Frees all of the allocated memory used in main
 *
 * @param line The read-in line 
 * @param lincopy The copy of line
 * @param args The array of commands
 * @param numTokens The number or tokens in args
 * @return 0 for success, -1 otherwise
 */
int freeAll(char *line, char *linecopy, char **args, int numTokens);

/**
 * Executes the built-in Change Directory ("cd") command
 *
 * @param args The array of commands
 */
int built_in_CD(char **args);

/**
 * Executes the built-in Jobs ("jobs") command
 *
 * @param joblist The list structure containing our jobs
 * @return 0 for success, -1 otherwise
 */
int built_in_JOBS(struct list *joblist);

/**
 * Check each job's status in the joblist and updates them and the joblist as a whole.
 *
 * @param joblist The list structure containing our jobs
 * @param verified A pointer to a boolean flag for if the list should be cleared
 * @param allDone A pointer to a boolean flag for if every job in the list has terminated
 * @param index The next available index in the joblist
 * @return 0 on success, -1 otherwise
 */
int updateJoblist(struct list *joblist, int *verified, int *alldone, int *index);

#endif	/* __MYDASH_H */
