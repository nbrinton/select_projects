#include	<sys/types.h>			// Required for pid_t etc types
#include	<sys/wait.h>			// Required to use the wait function
#include	"mydash.h"
#include	<sys/types.h>			// Required for some of our prototypes for convenience
#include	<stdio.h>
#include	<stdlib.h>			
#include	<sysexits.h>			// Required for using the exit function
#include	<string.h>				// Required for common string functions
#include	<unistd.h>				// Required for pid_t etc types
#include	<ncurses.h>				// Required for using ncurses library function calls?
#include	<readline/readline.h>	// Required for readline
#include	<readline/history.h>	// Required for readline
#include	<pwd.h>					// Required for cd command functionality
#include	<ctype.h>				// Required for isspace method used in white()
#include "../../p0/libsrc/List.h"	// Required for using my linked-list
#include "../../p0/libsrc/Node.h"	// Required for using my linked-list
#include	<job.h>					// Required for using my job struct

//#define DEBUG 0
#define TRUE 1
#define FALSE 0


// Valgrind: work on finding memory leaks by checking strings for null termination and also check for variable initializations
const int MAX_TOKENS = 2048;//or 2049 to include the base command?

char **parseInput(char *str, char *delimiter, int *numTokens);
int white(char *line);
const char* git_version(void);
int isBackground(char **args, int numTokens);
int freeAll(char *line, char *linecopy, char **args, int numTokens);
int built_in_CD(char **args);
int built_in_JOBS(struct list *joblist);
int updateJoblist(struct list *joblist, int *verified, int *alldone, int *index);


int main(int argc, char **argv) {
	
	// If the user runs ./mydash with the -v argument, print the version and quit
	if (argv[1] != NULL && strcmp(argv[1], "-v") == 0) {
		printf("MyDash Version: %s\n", git_version());
		exit(EXIT_SUCCESS);
	}

	pid_t pid;
	int status;
	char *line;
	char *prompt = getenv("DASH_PROMPT");
	char **args;//Array of strings of the command and each flag
	int numTokens = 0;//Number of tokens in args
	char *delimiter = " ";//Parsing delimiter
	struct list *joblist = createList(equals, toString, freeJob);//Linked list of our jobs
	int index = 0;// represents the next available index in joblist
	int verified = FALSE;// Boolean value for if the user has hit enter to refresh the joblist
	int allDone = FALSE;// Boolean value for if all jobs in joblist are done executing

	if (prompt == NULL) {
		prompt = "mydash> ";
	}

	using_history();//readline call to store command history

	while ((line = readline(prompt))) {
		verified = TRUE;// If flag is here, the list refreshes after anything
		updateJoblist(joblist, &verified, &allDone, &index);// Update all jobs in the list
		
		// If the user just hits enter, free line and continue to the next iteration of this while-loop.
		if (strcmp(line, "") == 0) {
			if (line != NULL) free(line);
			
			// If our joblist isn't empty
			if (isEmpty(joblist) == FALSE) {
				// If all the jobs are done
				if(allDone == TRUE) {
					//verified = TRUE;// If flag is here, then the list only refreshes when the user only hits enter
					updateJoblist(joblist, &verified, &allDone, &index);
				}
			}
			continue;
		}
		
		// If the line contains only whitespace, then free line and continue to the next iteration of this while-loop.
		if (white(line) == 1) {
			if (line != NULL) free(line);
			continue;
		}
		
		add_history(line);
		char *linecopy = (char *) malloc(sizeof(char) * strlen(line) + 1);
		strcpy(linecopy, line);//copy the entered line since tokenizing it will destroy it
		args = parseInput(linecopy, delimiter, &numTokens);
		int runInBG = isBackground(args, numTokens);// Check to see if this job needs to be run in the background
		#ifdef DEBUG
			//print contents of args
			int i;
			for (i = 0; i < numTokens; i++) {
				fprintf(stderr, "args[%d] = %s\n", i, args[i]);
			}
		#endif

		// BUILT-IN COMMANDS
		if (strcmp(args[0], "exit") == 0) {		// exit command
			freeAll(line, linecopy, args, numTokens);
			freeList(joblist);
			exit(EXIT_SUCCESS);
		} else if (strcmp(args[0], "cd") == 0) {	// cd command
			built_in_CD(args);
		} else if (strcmp(args[0], "jobs") == 0) {	// jobs command
			built_in_JOBS(joblist);
		} else {
		// FORK/EXEC
			
			// Enable colors for the ls command
			if (strcmp(args[0], "ls") == 0) {
				args[numTokens + 1] = NULL;
				args[numTokens] = "--color=auto";
			}

			pid = fork();//Fork to create the child process on which this command will execute
			if (pid < 0) {
				fprintf(stderr, "fork error\n");// -1 is returned if there is an error forking
			} else if (pid == 0) {
				execvp(args[0], args);// Search for and execute the given command
				freeAll(line, linecopy, args, numTokens);
				freeList(joblist);
				exit(EXIT_SUCCESS);// If the command does not exist, terminate this child process
			}
			
			if (runInBG == FALSE && (pid = waitpid(pid, &status, 0)) < 0) {
				fprintf(stderr, "waitpid error");
			} else if (runInBG == TRUE && waitpid(pid, &status, WNOHANG) == 0) {	// Tells calling process to continue running
				index++;
				struct job *job1 = createJob(index, pid, line);
				struct node *n = createNode(job1);
				char *tmp = jobStart(job1);
				fprintf(stderr, "%s\n", tmp);
				addAtRear(joblist, n);
				free(tmp);
			}
		}
		freeAll(line, linecopy, args, numTokens);
	}
	
	freeList(joblist);
	exit(EXIT_SUCCESS);

}


// HELPER FUNCTIONS

/*
 * Source: CS253 Source Files (modified)
 */
char **parseInput(char *str, char *delimiter, int *numTokens) {
	char *nextToken;
	char **tokens = (char **) calloc (MAX_TOKENS, sizeof(char *));//used to be malloc

	// tokenize the given string str
	nextToken = strtok(str, delimiter);
	*numTokens = 0;
	while (nextToken != NULL) {
		tokens[*numTokens] = (char *) malloc(sizeof(char) * (strlen(nextToken) + 1));//NOTE: +1 allows for null-terminating character
		strcpy(tokens[*numTokens], nextToken);
		(*numTokens)++;
		nextToken = strtok(NULL, delimiter);
	}
	//Now the tokens are copied into token[0..numTokens-1];
	tokens[*numTokens] = NULL;
	return tokens;
}

/*
 * This function returns true if this line only contains whitespace
 */
int white(char *line) {
	int i;
	for (i = 0; i < strlen(line); i++) {
		if (isspace(line[i]) == 0) {
			return FALSE;
		}
	}
	return TRUE;
}

/*
 * Checks to see if the command needs to be run in the background
 */
int isBackground(char **args, int numTokens) {
	char *lastChar = strstr(args[numTokens - 1], "&");
	int runInBG = FALSE;
	if (lastChar != NULL) {
		runInBG = TRUE;
		if (strcmp(args[numTokens - 1], "&") != 0) {
			char *sans = args[numTokens - 1];
			sans[strlen(sans) - 1] = '\0';
		} else {
			args[numTokens - 1] = NULL;
		}
	}
	
	return runInBG;
}

/*
 * "cd" command
 */
int built_in_CD(char **args) {
	if (args[1] == NULL) {
		// Using the pwd library
		struct passwd* currentDirectory = getpwuid(getuid());//Grab the user's current working directory
		chdir(currentDirectory->pw_dir);//pwd automatically generates pw_dir (home) directory with the above call
	} else if (args[1] != NULL) {
		chdir(args[1]);
	}
	return 0;//exit successa
}

/*
 * "jobs" command
 */
int built_in_JOBS(struct list *joblist) {
	printList(joblist);
	return 0;
}

/*
 * Frees all allocated memory
 * NOTE: run valgrind --leak-check=full ./mydash 
 * NOTE: readline creates many erroneous memory leak error messages in Valgrind. Ignore these
 */
int freeAll(char *line, char *linecopy, char **args, int numTokens) {
	if (line != NULL) {
		free(line);
	}
	if (linecopy != NULL) {
		free(linecopy);
	}
	if (args != NULL) {
		int i;
		for(i = 0; i < numTokens; i++) {
			free(args[i]);
		}
		free(args);
	}
	return 0;//exit success
}

/*
 * Check each job's status in the joblist and update them and the joblist as a whole
 */
int updateJoblist(struct list *joblist, int *verified, int *allDone, int *index) {
	struct node *current = joblist->head;
	*allDone = TRUE;
	while (current != NULL) {
		struct job *currentJob = (struct job *)current->obj;
		updateJob(currentJob);
		// if (currentJob is not done) allDone = FALSE
		if (currentJob->status != 0) {
			*allDone = FALSE;
		}
		current = current->next;
	}
	
	// If all of the jobs are done and the user has hit enter, then print the list, clear it, and reset index and flags
	if (*allDone == TRUE && *verified == TRUE) {
		printList(joblist);
		*index = 0;
		*verified = FALSE;
		*allDone = FALSE;
		struct node *clear = joblist->head;
		while (clear != NULL) {
			struct node *tmp = clear->next;
			struct node *removed = removeNode(joblist, clear);
			freeNode(removed, freeJob);
			clear = tmp;
		}
	}
	return 0;
}
