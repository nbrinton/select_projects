/*
	job.h: Defines the interface for an job to be stored in a node
	of our doubly-linked list.
*/
#ifndef __JOB_H
#define __JOB_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

//typedef enum {false, true} boolean;

#define MAX_KEY_DIGITS 10 /* max digits in an int */

struct job {
	int index;
	pid_t id;
	char *command;
	int status;
};

/**
 * Constructor: Allocates a new job structure and initializes its members.
 *
 * @return a pointer to the allocated job.
 */
struct job* createJob (int index, pid_t id, const char *command);

/**
 * Returns a string representation of the given job.
 *
 * @param job The job to create the string for.
 * @return The string representation of this job. The user of this
 *			function is responsible for freeing the returned string.
 */
char *toString(const void *job);

/**
 * Special version of toString used when we start the new job
 */
char *jobStart(const void *job);

/**
 * Frees the data stored in the given job and the job itself.
 *
 * Does nothing if job is <code>NULL</code>.
 *
 * @param job A pointer to the <code>struct job</code> to free.
 */
void freeJob(void *job);

/**
 * Checks if the given jobs are equal. Jobs are considered
 * equal if they have the same key.
 *
 * @param job1 A pointer to the first <code>struct job</code>.
 * @param jobs2 A pointer to the first <code>struct job</code>.
 * @return Returns 1 (true) if the jobs are equal, 0 (false)) otherwise.
 */
int equals(const void *job1,const void *job2);

/**
 * Checks to see if the job is still executing, and updates its status accordingly
 */
int updateJob(void *job);

#endif /* __JOB_H */
