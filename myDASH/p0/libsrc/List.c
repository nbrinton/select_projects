#include <stdio.h>
#include <stdlib.h>
#include "List.h"

struct list * createList(int (*equals)(const void *,const void *),
						 char * (*toString)(const void *),
						 void (*freeObject)(void *))
{
	struct list *list;
	list = (struct list *) malloc(sizeof(struct list));
	list->size = 0;
	list->head = NULL;
	list->tail = NULL;
	list->equals = equals;
	list->toString = toString;
	list->freeObject = freeObject;
	return list;
}

/**
 * Frees all elements of the given list and the <code>struct list *</code> itself.
 * Does nothing if list is <code>NULL</code>.
 *
 * @param list a pointer to a <code>struct list</code>.
 */
void freeList(struct list *list) {
	struct node *current = list->head;
	struct node *tmp = NULL;
	while (current != NULL) {
		tmp = current->next;
		freeNode(current, list->freeObject);
		current = tmp;
	}
	
	free(list);
}

/**
 * Returns the size of the given list. Returns 0 if the list is <code>NULL</code>.
 *
 * @param list a pointer to a <code>struct list</code>.
 * @return The current size of the list.
 */
int getSize(const struct list *list) {
	if(list == NULL) return 0;
	
	return list->size;
}

/**
 * Checks if the list is empty.
 *
 * @param  list a pointer to a <code>struct list</code>.
 * @return true if the list is empty; false otherwise.
 */
int isEmpty(const struct list *list) {
	return list->size == 0;
}

/**
 * Adds a node to the front of the list. After this method is called,
 * the given node will be the head of the list. (Node must be allocated
 * before it is passed to this function.) If the list and/or node are NULL,
 * the function will do nothing and return.
 *
 * @param list a pointer to a <code>struct list</code>.
 * @param node a pointer to the node to add.
 */
void addAtFront(struct list *list, struct node *node) {
	if (list == NULL) return;
	if (node == NULL) return;
	list->size++;
	node->next = list->head;
	node->prev = NULL;
	if (list->head == NULL) {
		list->head = node;
		list->tail = node;
	} else {
		list->head->prev = node;
		list->head = node;
	}
}

/**
 * Adds a node to the rear of the list. After this method is called,
 * the given node will be the tail of the list. (Node must be allocated
 * before it is passed to this function.) If the list and/or node are NULL,
 * the function will do nothing and return.
 *
 * @param list a pointer to a <code>struct list</code>.
 * @param node a pointer to the node to add.
 */
void addAtRear(struct list *list, struct node *node) {
	if (list == NULL) return;
	if (node == NULL) return;
	list->size++;
	node->prev = list->tail;
	node->next = NULL;
	if (list->tail == NULL){	//or list->head
		list->head = node;
		list->tail = node;
	} else {
		list->tail->next = node;
		list->tail = node;
	}
}

/**
 * Removes the node from the front of the list (the head node) and returns
 * a pointer to the node that was removed. If the list is NULL or empty,
 * the function will do nothing and return NULL.
 *
 * @param list a pointer to a <code>struct list</code>.
 * @return a pointer to the node that was removed.
 */
struct node* removeFront(struct list *list) {
	if (list == NULL) return NULL;
	if (list->size == 0) return NULL;

	struct node *target = list->head;

	if (list->size == 1) {
		list->head = NULL;
		list->tail = NULL;
	} else {
		list->head = list->head->next;
		list->head->prev = NULL;
	}
	target->next = NULL;
	target->prev = NULL;//should be unecessary but is a good safety measure
	list->size--;
	return target;
}

/**
 * Removes the node from the rear of the list (the tail node) and returns
 * a pointer to the node that was removed. If the list is NULL or empty,
 * the function will do nothing and return NULL.
 *
 * @param list a pointer to a <code>struct list</code>.
 * @return a pointer to the node that was removed.
 */
struct node* removeRear(struct list *list) {
	if (list == NULL) return NULL;
	if (list->size == 0) return NULL;

	struct node *target = list->tail;

	if (list->size == 1) {
		list->head = NULL;
		list->tail = NULL;	
	} else {
		list->tail = target->prev;
		list->tail->next = NULL;
	}

	target->next = NULL;//Should be unecessary but is always a good safety measure
	target->prev = NULL;
	list->size--;
	return target;
}

/**
 * Removes the node pointed to by the given node pointer from the list and returns
 * the pointer to it. Assumes that the node is a valid node in the list. If the node
 * pointer is NULL, the function will do nothing and return NULL.
 *
 * @param list a pointer to a <code>struct list</code>.
 * @param node a pointer to the node to remove.
 * @return a pointer to the node that was removed.
 */
struct node* removeNode(struct list *list, struct node *node) {
	if (list == NULL) return NULL;
	if (list->size == 0) return NULL;
	if (node == NULL) return NULL;
	
	if (list->size == 1) {
		list->head = NULL;
		list->tail = NULL;
	} else {
		if (node == list->tail) {
			list->tail = list->tail->prev;
			list->tail->next = NULL;
		} else if (node == list->head) {
			list->head = list->head->next;
			list->head->prev = NULL;
		} else {
			node->next->prev = node->prev;
			node->prev->next = node->next;
		}
	}
	node->next = NULL;
	node->prev = NULL;
	list->size--;
	return node;
}

/**
 * Searches the list for a node with the given obj and returns the pointer to the
 * found node. The search method should call the equals function pointer that was
 * provided by the user of this library.
 *
 * @param list a pointer to a <code>struct list</code>.
 * @param the object to search for.
 * @return a pointer to the node that was found. Or <code>NULL</code> if a node with the given obj was not
 * found or the list is <code>NULL</code> or empty.
 */
struct node* search(const struct list *list, const void *obj){
	if (list == NULL) return NULL;
	if (list->size == 0) return NULL;
	if (obj == NULL) return NULL;

	struct node *current = list->head;
	while (current != NULL){
		if (list->equals(current->obj, obj)) {
			return current;
		} else {
			current = current->next;
		}
	}

	return NULL;//Or return current (which is NULL at this point too)
}

/**
 * Reverses the order of the given list.
 *
 * @param list a pointer to a <code>struct list</code>.
 */
void reverseList(struct list *list) {
	if (list == NULL) return;
	if (list->size == 0) return;
	if (list->size == 1) return;

	struct node *head = list->head;
	struct node *current = list->head;
	struct node *tmp = current;
	while(current != NULL){
		tmp = current->next;
		current->next = current->prev;
		current->prev = tmp;
		current = current->prev;
	}
	list->head = list->tail;
	list->tail = head;
}

/**
 * Prints the list.
 *
 * @param list a pointer to a <code>struct list</code>.
 */
void printList(const struct list *list) {
	if (!list) return; //list was null!!
	char *output;
	struct node *temp = list->head;
	while (temp) {
		output = list->toString(temp->obj);
		printf("%s\n",output);
		free(output);
		temp = temp->next;
	}
}
