/*
 * UnitTestList.c
 *
 *	  Author: marissa, nbrinton
 */

#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0


#include "Object.h"
#include "Node.h"
#include "List.h"

/*
 * macro to mimic the functionality of assert() from <assert.h>. The difference is that this version doesn't exit the program entirely.
 * It will just break out of the current function (or test case in this context).
 */
#define myassert(expr) if(!(expr)){ fprintf(stderr, "\t[assertion failed] %s: %s\n", __PRETTY_FUNCTION__, __STRING(expr)); return FALSE; }

struct list *testlist;

int testCount = 0;
int passCount = 0;

void printTestInfo(char* testName, char *info) {
	fprintf(stdout, "%s - %s\n", testName, info);
}

void printTestResult(char* testName, int passed) {
	if(passed) {
		fprintf(stdout, "%s - %s\n\n", "[PASSED]", testName);
	} else {
		fprintf(stdout, "%s - %s\n\n", "[FAILED]", testName);
	}
}

struct node *createTestNode(int jobid) {
	struct object * job = createObject(jobid, "cmd args");
	struct node *node = createNode(job);
	return node;
}

//REMOVEFRONT
int removeFrontWithNoNodes() {
	removeFront(testlist);
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeFrontWithOneNode() {
	struct node *a = createTestNode(1);
	addAtFront(testlist, a);
	removeFront(testlist);
	freeNode(a, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeFrontWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	removeFront(testlist);
	freeNode(a, (testlist->freeObject));
	myassert(testlist->size == 1);
	myassert(testlist->head == b);
	myassert(testlist->tail == b);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;
}

int removeFrontWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	removeFront(testlist);
	freeNode(a, (testlist->freeObject));
	myassert(testlist->size == 2);
	myassert(testlist->head == b);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == c);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	return TRUE;
}

int removeFrontTwiceWithOneNode() {
	struct node *a = createTestNode(1);
	addAtFront(testlist, a);
	removeFront(testlist);
	freeNode(a, (testlist->freeObject));
	removeFront(testlist);
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeFrontTwiceWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	removeFront(testlist);
	freeNode(a, (testlist->freeObject));
	removeFront(testlist);
	freeNode(b, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeFrontTwiceWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	removeFront(testlist);
	freeNode(a, (testlist->freeObject));
	removeFront(testlist);
	freeNode(b, (testlist->freeObject));
	myassert(testlist->size == 1);
	myassert(testlist->head == c);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;
}

int removeFrontThriceWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	removeFront(testlist);
	freeNode(a, (testlist->freeObject));
	removeFront(testlist);
	freeNode(b, (testlist->freeObject));
	removeFront(testlist);
	freeNode(c, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

//REMOVEREAR
int removeRearWithNoNodes() {
	removeRear(testlist);
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeRearWithOneNode() {
	struct node *a = createTestNode(1);
	addAtFront(testlist, a);
	removeRear(testlist);
	freeNode(a, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeRearWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	removeRear(testlist);
	freeNode(b, (testlist->freeObject));
	myassert(testlist->size == 1);
	myassert(testlist->head == a);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;
}

int removeRearWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	removeRear(testlist);
	freeNode(c, (testlist->freeObject));
	myassert(testlist->size == 2);
	myassert(testlist->head == a);
	myassert(testlist->tail == b);
	myassert(testlist->head->next == b);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == a);
	return TRUE;
}

int removeRearTwiceWithOneNode() {
	struct node *a = createTestNode(1);
	addAtFront(testlist, a);
	removeRear(testlist);
	freeNode(a, (testlist->freeObject));
	removeRear(testlist);
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeRearTwiceWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	removeRear(testlist);
	freeNode(b, (testlist->freeObject));
	removeRear(testlist);
	freeNode(a, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeRearTwiceWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	removeRear(testlist);
	freeNode(c, (testlist->freeObject));
	removeRear(testlist);
	freeNode(b, (testlist->freeObject));
	myassert(testlist->size == 1);
	myassert(testlist->head == a);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;
}

//REMOVENODE
int removeNodeWithNullNode() {
	struct node *a = createTestNode(1);
	struct node *nullNode = NULL;
	addAtFront(testlist, a);
	removeNode(testlist, nullNode);
	myassert(testlist->size == 1);
	myassert(testlist->head == a);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;
}

int removeNodeOnEmptyList() {
	struct node *a = createTestNode(1);
	removeNode(testlist, a);
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	freeNode(a, (testlist->freeObject));
	return TRUE;
}

int removeNodeWithOneNode() {
	struct node *a = createTestNode(1);
	addAtFront(testlist, a);
	removeNode(testlist, a);
	freeNode(a, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeNodeWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	removeNode(testlist, a);
	freeNode(a, (testlist->freeObject));
	myassert(testlist->size == 1);
	myassert(testlist->head == b);
	myassert(testlist->tail == b);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;
}

int removeNodeWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	removeNode(testlist, a);
	freeNode(a, (testlist->freeObject));
	myassert(testlist->size == 2);
	myassert(testlist->head == b);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == c);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	return TRUE;
}

int removeTwoNodesWithOneNode() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	removeNode(testlist, a);
	freeNode(a, (testlist->freeObject));
	removeNode(testlist, b);
	freeNode(b, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeTwoNodesWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	removeNode(testlist, a);
	freeNode(a, (testlist->freeObject));
	removeNode(testlist, b);
	freeNode(b, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeTwoNodesWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	removeNode(testlist, a);
	freeNode(a, (testlist->freeObject));
	removeNode(testlist, b);
	freeNode(b, (testlist->freeObject));
	myassert(testlist->size == 1);
	myassert(testlist->head == c);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;

}

int removeThreeNodesWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	removeNode(testlist, a);
	freeNode(a, (testlist->freeObject));
	removeNode(testlist, b);
	freeNode(b, (testlist->freeObject));
	removeNode(testlist, c);
	freeNode(c, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

int removeThreeNodesWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	removeNode(testlist, a);
	freeNode(a, (testlist->freeObject));
	removeNode(testlist, b);
	freeNode(b, (testlist->freeObject));
	removeNode(testlist, c);
	freeNode(c, (testlist->freeObject));
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL);
	return TRUE;
}

//ADDATFRONT
int addAtFrontWithNoNodes() {
	struct node *node = createTestNode(1);
	addAtFront(testlist, node);
	myassert(testlist->size == 1);
	myassert(testlist->head == node);
	myassert(testlist->tail == node);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	return TRUE;
}

int addAtFrontWithOneNode() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtFront(testlist, b);
	myassert(testlist->size == 2);
	myassert(testlist->head == b);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == a);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	return TRUE;
}

int addAtFrontWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, c);
	addAtFront(testlist, b);
	addAtFront(testlist, a);
	myassert(testlist->size == 3);
	myassert(testlist->head == a);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == c);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	myassert(testlist->tail->prev->prev == a);
	return TRUE;
}

int addAtFrontWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	struct node *d = createTestNode(4);
	addAtFront(testlist, d);
	addAtFront(testlist, c);
	addAtFront(testlist, b);
	addAtFront(testlist, a);
	myassert(testlist->size == 4);
	myassert(testlist->head == a);
	myassert(testlist->tail == d);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == c);
	myassert(testlist->head->next->next->next == d);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == c);
	myassert(testlist->tail->prev->prev == b);
	myassert(testlist->tail->prev->prev->prev == a);
	return TRUE;
}

//ADDATREAR
int addAtRearWithNoNodes() {
	struct node *a = createTestNode(1);
	addAtRear(testlist, a);
	myassert(testlist->size == 1);
	myassert(testlist->head == a);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;
}

int addAtRearWithOneNode() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	myassert(testlist->size == 2);
	myassert(testlist->head == a);
	myassert(testlist->tail == b);
	myassert(testlist->head->next == b);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == a);
	return TRUE;
}

int addAtRearWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	myassert(testlist->size == 3);
	myassert(testlist->head == a);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == c);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	myassert(testlist->tail->prev->prev == a);
	return TRUE;
}

int addAtRearWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	struct node *d = createTestNode(4);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	addAtRear(testlist, d);
	myassert(testlist->size == 4);
	myassert(testlist->head == a);
	myassert(testlist->tail == d);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == c);
	myassert(testlist->head->next->next->next == d);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == c);
	myassert(testlist->tail->prev->prev == b);
	myassert(testlist->tail->prev->prev->prev == a);
	return TRUE;
}

//REVERSE
int reverseListWithNullList() {
//	myassert(reverseList(testlist) == NULL);
	return TRUE;
}

int reverseListWithOneNode() {
	struct node *a = createTestNode(1);
	addAtFront(testlist, a);
	reverseList(testlist);
	myassert(testlist->size == 1);
	myassert(testlist->head == a);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	return TRUE;
}

int reverseListWithTwoNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	reverseList(testlist);
	myassert(testlist->size == 2);
	myassert(testlist->head == b);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == a);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	return TRUE;
}

int reverseListWithThreeNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	reverseList(testlist);
	myassert(testlist->size == 3);
	myassert(testlist->head == c);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == a);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	myassert(testlist->tail->prev->prev == c);
	return TRUE;
}

int reverseListWithFourNodes() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	struct node *d = createTestNode(4);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	addAtRear(testlist, d);
	reverseList(testlist);
	myassert(testlist->size == 4);
	myassert(testlist->head == d);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == c);
	myassert(testlist->head->next->next == b);
	myassert(testlist->head->next->next->next == a);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	myassert(testlist->tail->prev->prev == c);
	myassert(testlist->tail->prev->prev->prev == d);
	return TRUE;
}

//SEARCH
int searchNullList() {
	struct node *a = createTestNode(1);
	myassert(search(NULL, a->obj) == NULL);
	freeNode(a, (testlist->freeObject));
	return TRUE;
}

int searchEmptyList() {
	struct node *a = createTestNode(1);
	myassert(search(testlist, a->obj) == NULL);
	freeNode(a, (testlist->freeObject));
	return TRUE;
}

int searchForNullObject() {
	struct node *a = createTestNode(1);
	addAtFront(testlist, a);
	myassert(search(testlist, NULL) == NULL);
	return TRUE;
}

int searchOnListOfOne() {
	struct node *a = createTestNode(1);
	addAtFront(testlist, a);
	myassert(testlist->size == 1);
	myassert(testlist->head == a);
	myassert(testlist->tail == a);
	myassert(testlist->head->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == NULL);
	myassert(search(testlist, a->obj) == a);
	return TRUE;
}

int searchForHeadOnListOfTwo() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	myassert(testlist->size == 2);
	myassert(testlist->head == a);
	myassert(testlist->tail == b);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == a);
	myassert(testlist->tail->prev->prev == NULL);
	myassert(search(testlist, a->obj) == a);
	return TRUE;
}

int searchForTailOnListOfTwo() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	myassert(testlist->size == 2);
	myassert(testlist->head == a);
	myassert(testlist->tail == b);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == a);
	myassert(testlist->tail->prev->prev == NULL);
	myassert(search(testlist, b->obj) == b);
	return TRUE;

}

int searchForHeadOnListOfThree() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	myassert(testlist->size == 3);
	myassert(testlist->head == a);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == c);
	myassert(testlist->head->next->next->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	myassert(testlist->tail->prev->prev == a);
	myassert(testlist->tail->prev->prev->prev == NULL);
	myassert(search(testlist, a->obj) == a);
	return TRUE;
}

int searchForTailOnListOfThree() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	myassert(testlist->size == 3);
	myassert(testlist->head == a);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == c);
	myassert(testlist->head->next->next->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	myassert(testlist->tail->prev->prev == a);
	myassert(testlist->tail->prev->prev->prev == NULL);
	myassert(search(testlist, c->obj) == c);
	return TRUE;

}

int searchForMiddleOnListOfThree() {
	struct node *a = createTestNode(1);
	struct node *b = createTestNode(2);
	struct node *c = createTestNode(3);
	addAtFront(testlist, a);
	addAtRear(testlist, b);
	addAtRear(testlist, c);
	myassert(testlist->size == 3);
	myassert(testlist->head == a);
	myassert(testlist->tail == c);
	myassert(testlist->head->next == b);
	myassert(testlist->head->next->next == c);
	myassert(testlist->head->next->next->next == NULL);
	myassert(testlist->head->prev == NULL);
	myassert(testlist->tail->next == NULL);
	myassert(testlist->tail->prev == b);
	myassert(testlist->tail->prev->prev == a);
	myassert(testlist->tail->prev->prev->prev == NULL);
	myassert(search(testlist, b->obj) == b);
	return TRUE;
}

//NULLNODE
int nullNodeTest() {
	struct node *a = NULL;
	addAtFront(testlist, a);
	myassert(testlist->size == 0);
	myassert(testlist->head == NULL);
	myassert(testlist->tail == NULL); 
	return TRUE;
}

//END





void beforeTest(char* testName) {
	printTestInfo(testName, "Running...");
	testlist = createList(equals, toString, freeObject);
	testCount++;
}

void afterTest(char* testName, int result) {
	printTestResult(testName, result);
	freeList(testlist);
	passCount += result;
}

/*
 * TODO: Write your test functions here
 */
void runUnitTests() {
	int result;
	char *testName;

	//REMOVEFRONT
	testName = "removeFrontWithNoNodes";
	beforeTest(testName);
	result = removeFrontWithNoNodes();
	afterTest(testName, result);
	
	testName = "removeFrontWithOneNode";
	beforeTest(testName);
	result = removeFrontWithOneNode();
	afterTest(testName, result);
	
	testName = "removeFrontWithTwoNodes";
	beforeTest(testName);
	result = removeFrontWithTwoNodes();
	afterTest(testName, result);

	testName = "removeFrontWithThreeNodes";
	beforeTest(testName);
	result = removeFrontWithThreeNodes();
	afterTest(testName, result);

	testName = "removeFrontTwiceWithOneNode";
	beforeTest(testName);
	result = removeFrontTwiceWithOneNode();
	afterTest(testName, result);

	testName = "removeFrontTwiceWithTwoNodes";
	beforeTest(testName);
	result = removeFrontTwiceWithTwoNodes();
	afterTest(testName, result);

	testName = "removeFrontTwiceWithThreeNodes";
	beforeTest(testName);
	result = removeFrontTwiceWithThreeNodes();
	afterTest(testName, result);

	testName = "removeFrontThriceWithThreeNodes";
	beforeTest(testName);
	result = removeFrontThriceWithThreeNodes();
	afterTest(testName, result);
	
	//REMOVEREAR
	testName = "removeRearWithNoNodes";
	beforeTest(testName);
	result = removeRearWithNoNodes();
	afterTest(testName, result);
	
	testName = "removeRearWithOneNode";
	beforeTest(testName);
	result = removeRearWithOneNode();
	afterTest(testName, result);
	
	testName = "removeRearWithTwoNodes";
	beforeTest(testName);
	result = removeRearWithTwoNodes();
	afterTest(testName, result);
	
	testName = "removeRearWithThreeNodes";
	beforeTest(testName);
	result = removeRearWithThreeNodes();
	afterTest(testName, result);

	testName = "removeRearTwiceWithOneNode";
	beforeTest(testName);
	result = removeRearTwiceWithOneNode();
	afterTest(testName, result);

	testName ="removeRearTwiceWithTwoNodes";
	beforeTest(testName);
	result = removeRearTwiceWithTwoNodes();
	afterTest(testName, result);

	testName = "removeRearTwiceWithThreeNodes";
	beforeTest(testName);
	result = removeRearTwiceWithThreeNodes();
	afterTest(testName, result);

	//REMOVENODE
	testName = "removeNodeWithOneNode";
	beforeTest(testName);
	result = removeNodeWithOneNode();
	afterTest(testName, result);

	testName = "removeNodeWithNullNode";
	beforeTest(testName);
	result = removeNodeWithNullNode();
	afterTest(testName, result);

	testName ="removeNodeOnEmptyList";
	beforeTest(testName);
	result = removeNodeOnEmptyList();
	afterTest(testName, result);

	testName = "removeNodeWithTwoNodes";
	beforeTest(testName);
	result = removeNodeWithTwoNodes();
	afterTest(testName, result);
	
	testName = "removeNodeWithThreeNodes";
	beforeTest(testName);
	result = removeNodeWithThreeNodes();
	afterTest(testName, result);

	testName = "removeTwoNodesWithOneNode";
	beforeTest(testName);
	result = removeTwoNodesWithOneNode();
	afterTest(testName, result);
	
	testName = "removeTwoNodesWithTwoNodes";
	beforeTest(testName);
	result = removeTwoNodesWithTwoNodes();
	afterTest(testName, result);

	testName = "removeTwoNodesWithThreeNodes";
	beforeTest(testName);
	result = removeTwoNodesWithThreeNodes();
	afterTest(testName, result);
	
	testName = "removeThreeNodesWithTwoNodes";
	beforeTest(testName);
	result = removeThreeNodesWithTwoNodes();
	afterTest(testName, result);
	
	testName = "removeThreeNodesWithThreeNodes";
	beforeTest(testName);
	result = removeThreeNodesWithThreeNodes();
	afterTest(testName, result);

	//ADDATFRONT
	testName = "addAtFrontWithNoNodes";
	beforeTest(testName);
	result = addAtFrontWithNoNodes();
	afterTest(testName, result);

	testName = "addAtFrontWithOneNode";
	beforeTest(testName);
	result = addAtFrontWithOneNode();
	afterTest(testName, result);

	testName = "addAtFrontWithTwoNodes";
	beforeTest(testName);
	result = addAtFrontWithTwoNodes();
	afterTest(testName, result);
	
	testName = "addAtFrontWithThreeNodes";
	beforeTest(testName);
	result = addAtFrontWithThreeNodes();
	afterTest(testName, result);
	
	//ADDATREAR
	testName = "addAtRearWithNoNodes";
	beforeTest(testName);
	result = addAtRearWithNoNodes();
	afterTest(testName, result);

	testName = "addAtRearWithOneNode";
	beforeTest(testName);
	result = addAtRearWithOneNode();
	afterTest(testName, result);

	testName = "addAtRearWithTwoNodes";
	beforeTest(testName);
	result = addAtRearWithOneNode();
	afterTest(testName, result);
	
	testName = "addAtRearWithThreeNodes";
	beforeTest(testName);
	result = addAtRearWithThreeNodes();
	afterTest(testName, result);
	
	//REVERSE
	testName = "reverseListWithNullList";
	beforeTest(testName);
	result = reverseListWithNullList();
	afterTest(testName, result);
	
	testName = "reverseListWithOneNode";
	beforeTest(testName);
	result = reverseListWithOneNode();
	afterTest(testName, result);
	
	testName = "reverseListWithTwoNodes";
	beforeTest(testName);
	result = reverseListWithTwoNodes();
	afterTest(testName, result);
	
	testName = "reverseListWithThreeNodes";
	beforeTest(testName);
	result = reverseListWithThreeNodes();
	afterTest(testName, result);
	
	testName = "reverseListWithFourNodes";
	beforeTest(testName);
	result = reverseListWithFourNodes();
	afterTest(testName, result);

	//SEARCH
	testName = "searchNullList";
	beforeTest(testName);
	result = searchNullList();
	afterTest(testName, result);
	
	testName = "searchEmptyList";
	beforeTest(testName);
	result = searchEmptyList();
	afterTest(testName, result);

	testName = "searchForNullObject";
	beforeTest(testName);
	result = searchForNullObject();
	afterTest(testName, result);
	
	testName = "searchOnListOfOne";
	beforeTest(testName);
	result = searchOnListOfOne();
	afterTest(testName, result);

	testName = "searchForHeadOnListOfTwo";
	beforeTest(testName);
	result = searchForHeadOnListOfTwo();
	afterTest(testName, result);

	testName = "searchForTailOnListOfTwo";
	beforeTest(testName);
	result = searchForTailOnListOfTwo();
	afterTest(testName, result);

	testName = "searchForHeadOnListOfThree";
	beforeTest(testName);
	result = searchForHeadOnListOfThree();
	afterTest(testName, result);

	testName = "searchForTailOnListOfThree";
	beforeTest(testName);
	result = searchForTailOnListOfThree();
	afterTest(testName, result);

	testName = "searchForMiddleOnListOfThree";
	beforeTest(testName);
	result = searchForMiddleOnListOfThree();
	afterTest(testName, result);
	
	//NULLNODE
	testName = "nullNodeTest";
	beforeTest(testName);
	result = nullNodeTest();
	afterTest(testName, result);
	
	//RESULTS
	fprintf(stdout, "Test Cases: %d\n",  testCount);
	fprintf(stdout, "Passed: %d\n", passCount);
	fprintf(stdout, "Failed: %d\n", testCount - passCount);
}

int main(int argc, char *argv[]) {
	runUnitTests();
	exit(0);
}
