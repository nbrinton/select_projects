
#include <stdio.h>
#include <stdlib.h>
#include "ThreadsafeBoundedList.h"
#include <List.h>

/**
	Contains functions to manipulate a thread-safe bounded doubly-linked list.
*/

struct tsb_list {
    struct list *list;
    int capacity;
    int stop_requested;
    pthread_mutex_t mutex;
    pthread_cond_t listNotFull;
    pthread_cond_t listNotEmpty;
};


struct tsb_list * tsb_createList(int (*equals)(const void *, const void *),
		char *(*toString)(const void *),
		void (*freeObject)(void *),
		int capacity)
{
	struct tsb_list * newList = (struct tsb_list *) malloc(sizeof(struct tsb_list));
	newList->list = createList(equals, toString, freeObject);
	newList->capacity = capacity;
	newList->stop_requested = FALSE;
	pthread_mutex_init(&(newList->mutex), NULL);
	pthread_cond_init(&newList->listNotFull, NULL);
	pthread_cond_init(&newList->listNotEmpty, NULL);

	return newList;
}


/*
   	Called by producers after they are done to wake up waiting consumers
*/
void tsb_finishUp(struct tsb_list * monitor_list)
{
	// wake up each waiting consumer thread and terminate it as the producers aren't producing anymore
	if (!monitor_list) return;// if someone passes a null list
	pthread_mutex_lock(&monitor_list->mutex);
	monitor_list->stop_requested = TRUE;// set flag to let functions know that a stop has been requested
	//broadcast the listNotEmpty thread condition to break threads out of the wait while-loop and terminate
	pthread_cond_broadcast(&monitor_list->listNotEmpty);
	pthread_mutex_unlock(&monitor_list->mutex);
	return;
}

void tsb_freeList(struct tsb_list * monitor_list)
{
	if (!monitor_list) return;
	pthread_mutex_lock(&monitor_list->mutex);
	freeList(monitor_list->list);
	pthread_mutex_unlock(&monitor_list->mutex);
	free(monitor_list);
	return;
}


int tsb_getSize(struct tsb_list * monitor_list)
{
	if (!monitor_list) return 0;// if someone passes a null list
	// locking here is "defensive programming" because we can't see the source code and it might create side-effects
	pthread_mutex_lock(&monitor_list->mutex);
	int size = getSize(monitor_list->list);// Using the getSize function from p0's list on the monitor_list's list 
	pthread_mutex_unlock(&monitor_list->mutex);
	return size;
}


int tsb_getCapacity(struct tsb_list * monitor_list)
{
	if (!monitor_list) return 0;
	pthread_mutex_lock(&monitor_list->mutex);
	int capacity = monitor_list->capacity;
	pthread_mutex_unlock(&monitor_list->mutex);
	return capacity;
}


void tsb_setCapacity(struct tsb_list * monitor_list, int capacity)
{
	if (!monitor_list) return;
	pthread_mutex_lock(&monitor_list->mutex);
	monitor_list->capacity = capacity;
	pthread_mutex_unlock(&monitor_list->mutex);
	return;
}


Boolean tsb_isEmpty(struct tsb_list * monitor_list)
{
	if (!monitor_list) return 0;
	pthread_mutex_lock(&monitor_list->mutex);
	int retval = isEmpty(monitor_list->list);
	pthread_mutex_unlock(&monitor_list->mutex);
	return retval;
}


void tsb_addAtFront(struct tsb_list * monitor_list, struct node* node)
{
	if (!monitor_list || !node) return;
	pthread_mutex_lock(&monitor_list->mutex);
	while (monitor_list->list->size >= monitor_list->capacity) {
		// Puts this thread in the "waiting room" and passes the key (mutex, second arg) back to the list and sets the flag listNotFull
		pthread_cond_wait(&monitor_list->listNotFull, &monitor_list->mutex);
	}
	addAtFront(monitor_list->list, node);
	pthread_mutex_unlock(&monitor_list->mutex);
	pthread_cond_signal(&monitor_list->listNotEmpty);//signal is not empty;
	return;
}


void tsb_addAtRear(struct tsb_list * monitor_list, struct node* node)
{
	if (!monitor_list || !node) return;
	pthread_mutex_lock(&monitor_list->mutex);
	while (monitor_list->list->size >= monitor_list->capacity) {
		// Puts this thread in the "waiting room" and passes the key (mutex, second arg) back to the list and sets the flag listNotFull
		pthread_cond_wait(&monitor_list->listNotFull, &monitor_list->mutex);
	}
	addAtRear(monitor_list->list, node);
	pthread_mutex_unlock(&monitor_list->mutex);
	pthread_cond_signal(&monitor_list->listNotEmpty);//signal is not empty;
	return;
}

struct node* tsb_removeFront(struct tsb_list * monitor_list)
{
	struct node *rval = NULL;
	if (!monitor_list) return rval;
	pthread_mutex_lock(&monitor_list->mutex);
	while (!monitor_list->stop_requested && isEmpty(monitor_list->list)/* == TRUE*/) {
		// Puts this thread in the "waiting room" and passes the key (mutex, second arg) back to the list and sets the flag listNotFull
		pthread_cond_wait(&monitor_list->listNotEmpty, &monitor_list->mutex);
	}
	rval = removeFront(monitor_list->list);
	pthread_mutex_unlock(&monitor_list->mutex);
	pthread_cond_signal(&monitor_list->listNotFull);//signal is not empty;
	return rval;
}

struct node* tsb_removeRear(struct tsb_list * monitor_list)
{
	struct node *rval = NULL;
	if (!monitor_list) return rval;
	pthread_mutex_lock(&monitor_list->mutex);
	while (!monitor_list->stop_requested && isEmpty(monitor_list->list)/* == TRUE*/) {
		pthread_cond_wait(&monitor_list->listNotEmpty, &monitor_list->mutex);
	}
	rval = removeRear(monitor_list->list);
	pthread_mutex_unlock(&monitor_list->mutex);
	pthread_cond_signal(&monitor_list->listNotFull);//signal is not empty;
	return rval;
}



struct node* tsb_search(struct tsb_list * monitor_list, const void *obj)
{
	struct node *retval = NULL;
	if (!monitor_list) return retval;
	pthread_mutex_lock(&monitor_list->mutex);
	retval = search(monitor_list->list, obj);
	pthread_mutex_unlock(&monitor_list->mutex);
	return retval;
}


void tsb_reverseList(struct tsb_list * monitor_list)
{
	if (!monitor_list) return;
	pthread_mutex_lock(&monitor_list->mutex);
	reverseList(monitor_list->list);
	pthread_mutex_unlock(&monitor_list->mutex);
	return;
}


void tsb_printList(struct tsb_list * monitor_list)
{
	if (!monitor_list) return;
	pthread_mutex_lock(&monitor_list->mutex);
	printList(monitor_list->list);
	pthread_mutex_unlock(&monitor_list->mutex);
	return;
}

